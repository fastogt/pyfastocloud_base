About PyFastoCloud Base
===============

PyFastoCloud Base python files.

Dependencies
========
`setuptools pyfastogt`

Install
========
`python3 setup.py install`