from enum import IntEnum
from typing import Optional, List


MIN_COUNTRY_LENGTH = 2
MAX_COUNTRY_LENGTH = 2048
MIN_URI_LENGTH = 3
MAX_URI_LENGTH = 2048
MIN_PATH_LENGTH = 1
MAX_PATH_LENGTH = 255

DATE_JS_FORMAT = "%m/%d/%Y %H:%M:%S"

DEFAULT_HLS_PLAYLIST = "master.m3u8"
DEFAULT_LOCALE = "en"
AVAILABLE_LOCALES = DEFAULT_LOCALE, "ru"
AVAILABLE_LOCALES_PAIRS = [(DEFAULT_LOCALE, "English"), ("ru", "Russian")]


def is_valid_locale_code(code: str) -> bool:
    for locale in AVAILABLE_LOCALES_PAIRS:
        if locale[0] == code:
            return True

    return False


# limits
DEFAULT_CATCHUP_CHUNK_DURATION = 12
DEFAULT_CATCHUP_EXIT_TIME = 3600

MIN_TIMESHIFT_CHUNK_LIFE_TIME = 1
DEFAULT_TIMESHIFT_CHUNK_LIFE_TIME = 12 * 3600
MAX_TIMESHIFT_CHUNK_LIFE_TIME = 30 * 12 * 3600

MIN_TIMESHIFT_DELAY = MIN_TIMESHIFT_CHUNK_LIFE_TIME
DEFAULT_TIMESHIFT_DELAY = 3600
MAX_TIMESHIFT_DELAY = 30 * 12 * 3600

MIN_TIMESHIFT_CHUNK_DURATION = 1
DEFAULT_TIMESHIFT_CHUNK_DURATION = 120
MAX_TIMESHIFT_CHUNK_DURATION = 600

MIN_IARC = 0
MAX_IARC = 21
DEFAULT_IARC = 18

MIN_PRICE = 0.0
DEFAULT_PRICE = MIN_PRICE
MAX_PRICE = 1000.0

MAX_INTEGER_NUMBER = 1000000

DEFAULT_RELAY_VIDEO = False
DEFAULT_RELAY_AUDIO = False

DEFAULT_LOOP = False
DEFAULT_HAVE_VIDEO = True
DEFAULT_HAVE_AUDIO = True

MIN_VOLUME = 0
DEFAULT_VOLUME = 1
MAX_VOLUME = 10

MIN_CREDITS_COUNT = 1
DEFAULT_CREDITS_COUNT = MAX_INTEGER_NUMBER
MAX_CREDITS_COUNT = MAX_INTEGER_NUMBER

MIN_RESTART_ATTEMPTS = 1
DEFAULT_RESTART_ATTEMPTS = 10
MAX_RESTART_ATTEMPTS = MAX_INTEGER_NUMBER

MIN_AUTO_EXIT_TIME = 1
MAX_AUTO_EXIT_TIME = MAX_INTEGER_NUMBER

MIN_UPDATE_EPG_TIME = 3600
MAX_UPDATE_EPG_TIME = MAX_INTEGER_NUMBER

MIN_AUDIO_SELECT = 0
MAX_AUDIO_SELECT = MAX_INTEGER_NUMBER

MIN_AUDIO_CHANNELS_COUNT = 1
MAX_AUDIO_CHANNELS_COUNT = 8

TS_VIDEO_PARSER = "tsparse"
H264_VIDEO_PARSER = "h264parse"
H265_VIDEO_PARSER = "h265parse"
VP8_VIDEO_PARSER = "vp8parse"
VP9_VIDEO_PARSER = "vp9parse"
DEFAULT_VIDEO_PARSER = H264_VIDEO_PARSER

AAC_AUDIO_PARSER = "aacparse"
AC3_AUDIO_PARSER = "ac3parse"
MPEG_AUDIO_PARSER = "mpegaudioparse"
RAW_AUDIO_PARSER = "rawaudioparse"
OPUS_AUDIO_PARSER = "opusparse"
DEFAULT_AUDIO_PARSER = AAC_AUDIO_PARSER

AVAILABLE_VIDEO_PARSERS = [
    (TS_VIDEO_PARSER, "ts"),
    (H264_VIDEO_PARSER, "h264"),
    (H265_VIDEO_PARSER, "h265"),
    (VP8_VIDEO_PARSER, "vp8"),
    (VP9_VIDEO_PARSER, "vp9"),
]
AVAILABLE_AUDIO_PARSERS = [
    (MPEG_AUDIO_PARSER, "mpeg"),
    (AAC_AUDIO_PARSER, "aac"),
    (AC3_AUDIO_PARSER, "ac3"),
    (RAW_AUDIO_PARSER, "raw"),
    (OPUS_AUDIO_PARSER, "opus"),
]

EAVC_ENC = "eavcenc"
OPEN_H264_ENC = "openh264enc"
X264_ENC = "x264enc"
NV_H264_ENC = "nvh264enc"
NV_H265_ENC = "nvh265enc"
VAAPI_H264_ENC = "vaapih264enc"
VAAPI_MPEG2_ENC = "vaapimpeg2enc"
MFX_H264_ENC = "mfxh264enc"
VP8_ENC = "vp8enc"
VP9_ENC = "vp9enc"
X265_ENC = "x265enc"
MSDK_H264_ENC = "msdkh264enc"
DEFAULT_VIDEO_CODEC = X264_ENC

LAME_MP3_ENC = "lamemp3enc"
FAAC = "faac"
VOAAC_ENC = "voaacenc"
OPUS_ENC = "opusenc"
DEFAULT_AUDIO_CODEC = FAAC

AVAILABLE_VIDEO_CODECS = [
    (EAVC_ENC, "eav"),
    (OPEN_H264_ENC, "openh264"),
    (X264_ENC, "x264"),
    (NV_H264_ENC, "nvh264"),
    (NV_H265_ENC, "nvh265"),
    (VAAPI_H264_ENC, "vaapih264"),
    (VAAPI_MPEG2_ENC, "vaapimpeg2"),
    (MFX_H264_ENC, "mfxh264"),
    (VP8_ENC, "vp8"),
    (VP9_ENC, "vp9"),
    (X265_ENC, "x265"),
    (MSDK_H264_ENC, "msdkh264"),
]
AVAILABLE_AUDIO_CODECS = [
    (LAME_MP3_ENC, "mpeg"),
    (FAAC, "aac"),
    (VOAAC_ENC, "voaac"),
    (OPUS_ENC, "opus"),
]

DEFAULT_SERVICE_ROOT_DIR_PATH = "~/streamer"
DEFAULT_SERVICE_LOG_PATH_TEMPLATE_3SIS = "http://{0}:{1}/service/log/{2}"
DEFAULT_STREAM_LOG_PATH_TEMPLATE_3SIS = "http://{0}:{1}/stream/log/{2}"
DEFAULT_STREAM_PIPELINE_PATH_TEMPLATE_3SIS = "http://{0}:{1}/stream/pipeline/{2}"

DEFAULT_TEST_URL = "unknown://test"
DEFAULT_FAKE_URL = "unknown://fake"
DEFAULT_WEBRTC_URL = "unknown://webrtc"
DEFAULT_DISPLAY_URL = "unknown://display"
DEFAULT_RECORD_URL = "unknown://record"


def is_special_url(url: str):
    return (
        url == DEFAULT_TEST_URL
        or url == DEFAULT_DISPLAY_URL
        or url == DEFAULT_FAKE_URL
        or url == DEFAULT_WEBRTC_URL
        or url == DEFAULT_RECORD_URL
    )


DEFAULT_DEVICES_COUNT = 10

MIN_STREAM_NAME_LENGTH = 1
MAX_STREAM_NAME_LENGTH = 256

MIN_STREAM_TVG_NAME_LENGTH = 1
MAX_STREAM_TVG_NAME_LENGTH = 64
MIN_STREAM_TVG_ID_LENGTH = 1
MAX_STREAM_TVG_ID_LENGTH = 64
MIN_STREAM_ICON_LENGTH = MIN_URI_LENGTH
MAX_STREAM_ICON_LENGTH = MAX_URI_LENGTH
MIN_STREAM_DESCRIPTION_LENGTH = 1
MAX_STREAM_DESCRIPTION_LENGTH = 4096

ACTIVATION_KEY_LENGTH = 97

MAX_VIDEO_DURATION_MSEC = (60 * 60 * 1000) * 24 * 365

DEFAULT_COUNTRY = "US"
AVAILABLE_COUNTRIES = [
    ("AF", "Afghanistan"),
    ("AX", "Åland Islands"),
    ("AL", "Albania"),
    ("DZ", "Algeria"),
    ("AS", "American Samoa"),
    ("AD", "Andorra"),
    ("AO", "Angola"),
    ("AI", "Anguilla"),
    ("AQ", "Antarctica"),
    ("AG", "Antigua and Barbuda"),
    ("AR", "Argentina"),
    ("AM", "Armenia"),
    ("AW", "Aruba"),
    ("AU", "Australia"),
    ("AT", "Austria"),
    ("AZ", "Azerbaijan"),
    ("BS", "Bahamas"),
    ("BH", "Bahrain"),
    ("BD", "Bangladesh"),
    ("BB", "Barbados"),
    ("BY", "Belarus"),
    ("BE", "Belgium"),
    ("BZ", "Belize"),
    ("BJ", "Benin"),
    ("BM", "Bermuda"),
    ("BT", "Bhutan"),
    ("BO", "Bolivia), Plurinational State of"),
    ("BQ", "Bonaire), Sint Eustatius and Saba"),
    ("BA", "Bosnia and Herzegovina"),
    ("BW", "Botswana"),
    ("BV", "Bouvet Island"),
    ("BR", "Brazil"),
    ("IO", "British Indian Ocean Territory"),
    ("BN", "Brunei Darussalam"),
    ("BG", "Bulgaria"),
    ("BF", "Burkina Faso"),
    ("BI", "Burundi"),
    ("KH", "Cambodia"),
    ("CM", "Cameroon"),
    ("CA", "Canada"),
    ("CV", "Cape Verde"),
    ("KY", "Cayman Islands"),
    ("CF", "Central African Republic"),
    ("TD", "Chad"),
    ("CL", "Chile"),
    ("CN", "China"),
    ("CX", "Christmas Island"),
    ("CC", "Cocos (Keeling) Islands"),
    ("CO", "Colombia"),
    ("KM", "Comoros"),
    ("CG", "Congo"),
    ("CD", "Congo), the Democratic Republic of the"),
    ("CK", "Cook Islands"),
    ("CR", "Costa Rica"),
    ("CI", "Côte d'Ivoire"),
    ("HR", "Croatia"),
    ("CU", "Cuba"),
    ("CW", "Curaçao"),
    ("CY", "Cyprus"),
    ("CZ", "Czech Republic"),
    ("DK", "Denmark"),
    ("DJ", "Djibouti"),
    ("DM", "Dominica"),
    ("DO", "Dominican Republic"),
    ("EC", "Ecuador"),
    ("EG", "Egypt"),
    ("SV", "El Salvador"),
    ("GQ", "Equatorial Guinea"),
    ("ER", "Eritrea"),
    ("EE", "Estonia"),
    ("ET", "Ethiopia"),
    ("FK", "Falkland Islands (Malvinas)"),
    ("FO", "Faroe Islands"),
    ("FJ", "Fiji"),
    ("FI", "Finland"),
    ("FR", "France"),
    ("GF", "French Guiana"),
    ("PF", "French Polynesia"),
    ("TF", "French Southern Territories"),
    ("GA", "Gabon"),
    ("GM", "Gambia"),
    ("GE", "Georgia"),
    ("DE", "Germany"),
    ("GH", "Ghana"),
    ("GI", "Gibraltar"),
    ("GR", "Greece"),
    ("GL", "Greenland"),
    ("GD", "Grenada"),
    ("GP", "Guadeloupe"),
    ("GU", "Guam"),
    ("GT", "Guatemala"),
    ("GG", "Guernsey"),
    ("GN", "Guinea"),
    ("GW", "Guinea-Bissau"),
    ("GY", "Guyana"),
    ("HT", "Haiti"),
    ("HM", "Heard Island and McDonald Islands"),
    ("VA", "Holy See (Vatican City State)"),
    ("HN", "Honduras"),
    ("HK", "Hong Kong"),
    ("HU", "Hungary"),
    ("IS", "Iceland"),
    ("IN", "India"),
    ("ID", "Indonesia"),
    ("IR", "Iran), Islamic Republic of"),
    ("IQ", "Iraq"),
    ("IE", "Ireland"),
    ("IM", "Isle of Man"),
    ("IL", "Israel"),
    ("IT", "Italy"),
    ("JM", "Jamaica"),
    ("JP", "Japan"),
    ("JE", "Jersey"),
    ("JO", "Jordan"),
    ("KZ", "Kazakhstan"),
    ("KE", "Kenya"),
    ("KI", "Kiribati"),
    ("KP", "Korea), Democratic People's Republic of"),
    ("KR", "Korea), Republic of"),
    ("KW", "Kuwait"),
    ("KG", "Kyrgyzstan"),
    ("LA", "Lao People's Democratic Republic"),
    ("LV", "Latvia"),
    ("LB", "Lebanon"),
    ("LS", "Lesotho"),
    ("LR", "Liberia"),
    ("LY", "Libya"),
    ("LI", "Liechtenstein"),
    ("LT", "Lithuania"),
    ("LU", "Luxembourg"),
    ("MO", "Macao"),
    ("MK", "Macedonia), the former Yugoslav Republic of"),
    ("MG", "Madagascar"),
    ("MW", "Malawi"),
    ("MY", "Malaysia"),
    ("MV", "Maldives"),
    ("ML", "Mali"),
    ("MT", "Malta"),
    ("MH", "Marshall Islands"),
    ("MQ", "Martinique"),
    ("MR", "Mauritania"),
    ("MU", "Mauritius"),
    ("YT", "Mayotte"),
    ("MX", "Mexico"),
    ("FM", "Micronesia), Federated States of"),
    ("MD", "Moldova), Republic of"),
    ("MC", "Monaco"),
    ("MN", "Mongolia"),
    ("ME", "Montenegro"),
    ("MS", "Montserrat"),
    ("MA", "Morocco"),
    ("MZ", "Mozambique"),
    ("MM", "Myanmar"),
    ("NA", "Namibia"),
    ("NR", "Nauru"),
    ("NP", "Nepal"),
    ("NL", "Netherlands"),
    ("NC", "New Caledonia"),
    ("NZ", "New Zealand"),
    ("NI", "Nicaragua"),
    ("NE", "Niger"),
    ("NG", "Nigeria"),
    ("NU", "Niue"),
    ("NF", "Norfolk Island"),
    ("MP", "Northern Mariana Islands"),
    ("NO", "Norway"),
    ("OM", "Oman"),
    ("PK", "Pakistan"),
    ("PW", "Palau"),
    ("PS", "Palestinian Territory), Occupied"),
    ("PA", "Panama"),
    ("PG", "Papua New Guinea"),
    ("PY", "Paraguay"),
    ("PE", "Peru"),
    ("PH", "Philippines"),
    ("PN", "Pitcairn"),
    ("PL", "Poland"),
    ("PT", "Portugal"),
    ("PR", "Puerto Rico"),
    ("QA", "Qatar"),
    ("RE", "Réunion"),
    ("RO", "Romania"),
    ("RU", "Russian Federation"),
    ("RW", "Rwanda"),
    ("BL", "Saint Barthélemy"),
    ("SH", "Saint Helena), Ascension and Tristan da Cunha"),
    ("KN", "Saint Kitts and Nevis"),
    ("LC", "Saint Lucia"),
    ("MF", "Saint Martin (French part)"),
    ("PM", "Saint Pierre and Miquelon"),
    ("VC", "Saint Vincent and the Grenadines"),
    ("WS", "Samoa"),
    ("SM", "San Marino"),
    ("ST", "Sao Tome and Principe"),
    ("SA", "Saudi Arabia"),
    ("SN", "Senegal"),
    ("RS", "Serbia"),
    ("SC", "Seychelles"),
    ("SL", "Sierra Leone"),
    ("SG", "Singapore"),
    ("SX", "Sint Maarten (Dutch part)"),
    ("SK", "Slovakia"),
    ("SI", "Slovenia"),
    ("SB", "Solomon Islands"),
    ("SO", "Somalia"),
    ("ZA", "South Africa"),
    ("GS", "South Georgia and the South Sandwich Islands"),
    ("SS", "South Sudan"),
    ("ES", "Spain"),
    ("LK", "Sri Lanka"),
    ("SD", "Sudan"),
    ("SR", "Suriname"),
    ("SJ", "Svalbard and Jan Mayen"),
    ("SZ", "Swaziland"),
    ("SE", "Sweden"),
    ("CH", "Switzerland"),
    ("SY", "Syrian Arab Republic"),
    ("TW", "Taiwan), Province of China"),
    ("TJ", "Tajikistan"),
    ("TZ", "Tanzania), United Republic of"),
    ("TH", "Thailand"),
    ("TL", "Timor-Leste"),
    ("TG", "Togo"),
    ("TK", "Tokelau"),
    ("TO", "Tonga"),
    ("TT", "Trinidad and Tobago"),
    ("TN", "Tunisia"),
    ("TR", "Turkey"),
    ("TM", "Turkmenistan"),
    ("TC", "Turks and Caicos Islands"),
    ("TV", "Tuvalu"),
    ("UG", "Uganda"),
    ("UA", "Ukraine"),
    ("AE", "United Arab Emirates"),
    ("GB", "United Kingdom"),
    (DEFAULT_COUNTRY, "United States"),
    ("UM", "United States Minor Outlying Islands"),
    ("UY", "Uruguay"),
    ("UZ", "Uzbekistan"),
    ("VU", "Vanuatu"),
    ("VE", "Venezuela), Bolivarian Republic of"),
    ("VN", "Viet Nam"),
    ("VG", "Virgin Islands), British"),
    ("VI", "Virgin Islands), U.S."),
    ("WF", "Wallis and Futuna"),
    ("EH", "Western Sahara"),
    ("YE", "Yemen"),
    ("ZM", "Zambia"),
    ("ZW", "Zimbabwe"),
]


def is_valid_country_code(code: str) -> bool:
    for country in AVAILABLE_COUNTRIES:
        if country[0] == code:
            return True

    return False


def round_value(value: float, precision=2):
    return round(value, precision)


StreamId = str
VideoParser = str
AudioParser = str
VideoCodec = str
AudioCodec = str
BitRate = str

Volume = float

VideoFlip = int


class StreamStatus(IntEnum):
    INIT = 0
    STARTED = 1
    READY = 2
    PLAYING = 3
    FROZEN = 4
    WAITING = 5

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item


class ExitStatus(IntEnum):
    EXIT_SUCCESS = 0
    EXIT_FAILURE = 1

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item


class UserAgent(IntEnum):
    GSTREAMER = 0
    VLC = 1
    FFMPEG = 2
    WINK = 3
    CHROME = 4
    MOZILLA = 5
    SAFARI = 6

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class MlBackends(IntEnum):
    NVIDIA = 0

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class HlsType(IntEnum):
    HLS_PULL = 0
    HLS_PUSH = 1

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class HlsSinkType(IntEnum):
    HLSSINK = 0
    HLSSINK2 = 1

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class RtmpSinkType(IntEnum):
    RTMPSINK = 0
    RTMP2SINK = 1

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class RtmpSrcType(IntEnum):
    RTMPSRC = 0
    RTMP2SRC = 1

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class SrtMode(IntEnum):
    NONE = 0
    CALLER = 1
    LISTENER = 2
    RENDEZVOUS = 3

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class RtmpType(IntEnum):
    RTMP_CUSTOM = 0
    RTMP_AFREECATV = 1
    RTMP_BILIBILI = 2
    RTMP_BONGACAMS = 3
    RTMP_CAM4 = 4
    RTMP_CAMPLACE = 5
    RTMP_CAMSODA = 6
    RTMP_BREAKERSTV = 7
    RTMP_CHATURBATE = 8
    RTMP_DLIVE = 9
    RTMP_DOUYU = 10
    RTMP_FACEBOOK = 11
    RTMP_FC2 = 12
    RTMP_FLIRT4FREE = 13
    RTMP_GOODGAME = 14
    RTMP_HUYA = 15
    RTMP_KAKAOTV = 16
    RTMP_MIXER = 17
    RTMP_MYFREECAMS = 18
    RTMP_NAVERTV = 19
    RTMP_NIMOTV = 20
    RTMP_ODNOKLASSNIKI = 21
    RTMP_PERISCOPE = 22
    RTMP_PICARTO = 23
    RTMP_SMASHCAST = 24
    RTMP_STREAMRAY = 25
    RTMP_STRIPCHAT = 26
    RTMP_TWITCH = 27
    RTMP_VAUGHNLIVE = 28
    RTMP_VIMEO = 29
    RTMP_VIRTWISH = 30
    RTMP_STEAM = 31
    RTMP_STEAMMATE = 32
    RTMP_VKONTAKTE = 33
    RTMP_VLIVE = 34
    RTMP_XLOVECAM = 35
    RTMP_YOUTUBE = 36
    RTMP_ZHANQITV = 37

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class MessageType(IntEnum):
    TEXT = 0
    HYPERLINK = 1

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class PlayerMessage:
    message = str()
    ttl = 0
    type = MessageType.TEXT

    def __init__(self, message: str, ttl: int, message_type: MessageType):
        self.message = message
        self.ttl = ttl
        self.type = message_type


class StreamType(IntEnum):
    PROXY = 0
    VOD_PROXY = 1
    RELAY = 2
    ENCODE = 3
    TIMESHIFT_PLAYER = 4
    TIMESHIFT_RECORDER = 5
    CATCHUP = 6
    TEST_LIFE = 7
    VOD_RELAY = 8
    VOD_ENCODE = 9
    COD_RELAY = 10
    COD_ENCODE = 11
    EVENT = 12
    CV_DATA = 13
    CHANGER_RELAY = 14
    CHANGER_ENCODE = 15

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class VodType(IntEnum):
    VODS = 0
    SERIES = 1

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class StreamLogLevel(IntEnum):
    LOG_LEVEL_EMERG = 0
    LOG_LEVEL_ALERT = 1
    LOG_LEVEL_CRIT = 2
    LOG_LEVEL_ERR = 3
    LOG_LEVEL_WARNING = 4
    LOG_LEVEL_NOTICE = 5
    LOG_LEVEL_INFO = 6
    LOG_LEVEL_DEBUG = 7

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class QualityPrefer(IntEnum):
    QP_AUDIO = 0
    QP_VIDEO = 1
    QP_BOTH = 2

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class BackgroundEffectType(IntEnum):
    BLUR = 0
    IMAGE = 1
    COLOR = 2

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class OverlayUrlType(IntEnum):
    URL = 0
    WPE = 1
    CEF = 2

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class AudioStabilizationType(IntEnum):
    DENOISER = 0
    DEREVERB = 1
    DEREVERB_DENOISER = 2

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class GpuModel(IntEnum):
    NVIDIA_A100 = 0
    NVIDIA_A10 = 1
    NVIDIA_T4 = 2
    NVIDIA_V100 = 3

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class AlphaMethodType(IntEnum):
    SET = 0
    GREEN = 1
    BLUE = 2
    CUSTOM = 3

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class Wpe:
    GL = "gl"

    def __init__(self, gl: bool) -> None:
        self.gl = gl

    def to_dict(self) -> dict:
        d = dict()

        d[Wpe.GL] = self.gl

        return d


class BackgroundColor(IntEnum):
    CHECKER = 0
    BLACK = 1
    WHITE = 2
    TRANSPARENT = 3

    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls(int(item)) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class StreamLink:
    HTTP_PROXY = "http_proxy"
    HTTPS_PROXY = "https_proxy"
    PREFER = "prefer"

    def __init__(
        self,
        prefer: QualityPrefer,
        http_proxy: Optional[str] = None,
        https_proxy: Optional[str] = None,
    ) -> None:
        self.http_proxy = http_proxy
        self.https_proxy = https_proxy
        self.prefer = prefer

    def to_dict(self) -> dict:
        d = dict()

        d[StreamLink.PREFER] = self.prefer
        if self.http_proxy is not None:
            d[StreamLink.HTTP_PROXY] = self.http_proxy
        if self.https_proxy is not None:
            d[StreamLink.HTTPS_PROXY] = self.https_proxy

        return d


class WhipProp:
    AUTH_TOKEN = "auth_token"

    def __init__(self, authtoken: Optional[str] = None) -> None:
        self.authtoken = authtoken

    def to_dict(self) -> dict:
        d = dict()
        if self.authtoken is not None:
            d[WhipProp.AUTH_TOKEN] = self.authtoken

        return d


class WhepProp:
    AUTH_TOKEN = "auth_token"

    def __init__(self, authtoken: Optional[str] = None) -> None:
        self.authtoken = authtoken

    def to_dict(self) -> dict:
        d = dict()
        if self.authtoken is not None:
            d[WhepProp.AUTH_TOKEN] = self.authtoken

        return d


class SrtKey:
    PASSPHRASE = "passphrase"
    KEY_LEN = "key_len"

    def __init__(self, passphrase: str, key_len: int) -> None:
        self.passphrase = passphrase
        self.key_len = key_len

    def to_dict(self) -> dict:
        d = dict()

        d[SrtKey.PASSPHRASE] = self.passphrase
        d[SrtKey.KEY_LEN] = self.key_len

        return d


class WebRTCProp:
    STUN = "stun"
    TURN = "turn"

    def __init__(self, stun: str, turn: str) -> None:
        self.stun = stun
        self.turn = turn

    def to_dict(self) -> dict:
        d = dict()

        d[WebRTCProp.STUN] = self.stun
        d[WebRTCProp.TURN] = self.turn

        return d


class Programme:
    CHANNEL = "channel"
    START = "start"
    STOP = "stop"
    TITLE = "title"
    CATEGORY = "category"
    DESCRIPTION = "desc"

    def __init__(
        self,
        channel: str,
        start: int,
        stop: int,
        title: str,
        category: Optional[str] = None,
        description: Optional[str] = None,
    ) -> None:
        self.channel = channel
        self.start = start
        self.stop = stop
        self.title = title
        self.category = category
        self.description = description

    def to_dict(self) -> dict:
        d = dict()

        d[Programme.CHANNEL] = self.channel
        d[Programme.START] = self.start
        d[Programme.STOP] = self.stop
        d[Programme.TITLE] = self.title
        if self.category is not None:
            d[Programme.CATEGORY] = self.category
        if self.DESCRIPTION is not None:
            d[Programme.DESCRIPTION] = self.description

        return d


class NdiProp:
    NDI_NAME_FIELD = "ndi_name"

    def __init__(self, ndi_name: str) -> None:
        self.ndi_name = ndi_name

    def __eq__(self, o: 'NdiProp') -> bool:
        return self.ndi_name == o.ndi_name

    def to_dict(self) -> dict:
        d = dict()

        d[NdiProp.NDI_NAME_FIELD] = self.ndi_name

        return d


class DrmKey:
    KID = "kid"
    KEY = "key"

    def __init__(self, kid: str, key: str) -> None:
        self.kid = kid
        self.key = key

    def __eq__(self, o: 'DrmKey') -> bool:
        return self.kid == o.kid and self.key == o.key

    def to_dict(self) -> dict:
        d = dict()

        d[DrmKey.KID] = self.kid
        d[DrmKey.KEY] = self.key

        return d


class Cef:
    GPU = "gpu"

    def __init__(self, gpu: bool) -> None:
        self.gpu = gpu

    def __eq__(self, o: 'Cef') -> bool:
        return self.gpu == o.gpu

    def to_dict(self) -> dict:
        d = dict()

        d[Cef.GPU] = self.gpu

        return d


class S3Prop:
    ACCESS_KEY = "access_key"
    SECRET_KEY = "secret_key"

    def __init__(self, access_key: str, secret_key: str) -> None:
        self.access_key = access_key
        self.secret_key = secret_key

    def __eq__(self, o: 'S3Prop') -> bool:
        return self.access_key == o.access_key and self.secret_key == o.secret_key

    def to_dict(self) -> dict:
        d = dict()

        d[S3Prop.ACCESS_KEY] = self.access_key
        d[S3Prop.SECRET_KEY] = self.secret_key

        return d


class InputUri:
    ID = "id"
    URI = "uri"
    USER_AGENT = "user_agent"
    STREAM_LINK = "stream_link"
    PROXY = "proxy"
    WPE = "wpe"
    CEF = "cef"
    KEYS = "keys"
    PROGRAM_NUMBER = "program_number"
    MULTICAST_IFACE = "multicast_iface"
    SRT_MODE = "srt_mode"
    SRT_KEY = "srt_key"
    RTMP_SRT_TYPE = "rtmpsrc_type"
    NDI = "ndi"
    AWS = "aws"
    WEB_RTC = "webrtc"
    PROGRAMME = "programme"

    def __init__(
        self,
        id: int,
        uri: str,
        user_agent: Optional[UserAgent] = None,
        stream_link: Optional[StreamLink] = None,
        proxy: Optional[str] = None,
        keys: Optional[List[DrmKey]] = None,
        wpe: Optional[Wpe] = None,
        cef: Optional[Cef] = None,
        program_number: Optional[int] = None,
        multicast_iface: Optional[str] = None,
        srt_mode: Optional[SrtMode] = None,
        srt_key: Optional[SrtKey] = None,
        rtmp_src_type: Optional[RtmpSrcType] = None,
        ndi: Optional[NdiProp] = None,
        aws: Optional[S3Prop] = None,
        web_rtc: Optional[WebRTCProp] = None,
        programme: Optional[Programme] = None,
    ) -> None:
        self.id = id
        self.uri = uri
        self.user_agent = user_agent
        self.stream_link = stream_link
        self.proxy = proxy
        self.keys = keys
        self.wpe = wpe
        self.cef = cef
        self.program_number = program_number
        self.multicast_iface = multicast_iface
        self.srt_mode = srt_mode
        self.srt_key = srt_key
        self.rtmp_src_type = rtmp_src_type
        self.ndi = ndi
        self.aws = aws
        self.web_rtc = web_rtc
        self.programme = programme

    def to_dict(self) -> dict:
        d = dict()

        d[InputUri.ID] = self.id
        d[InputUri.URI] = self.uri
        if self.user_agent is not None:
            d[InputUri.USER_AGENT] = self.user_agent
        if self.stream_link is not None:
            d[InputUri.STREAM_LINK] = self.stream_link.to_dict()
        if self.proxy is not None:
            d[InputUri.PROXY] = self.proxy
        if self.keys is not None:
            d[InputUri.KEYS] = [key.to_dict() for key in self.keys]
        if self.wpe is not None:
            d[InputUri.WPE] = self.wpe.to_dict()
        if self.cef is not None:
            d[InputUri.CEF] = self.cef.to_dict()
        if self.program_number is not None:
            d[InputUri.PROGRAM_NUMBER] = self.program_number
        if self.multicast_iface is not None:
            d[InputUri.MULTICAST_IFACE] = self.multicast_iface
        if self.srt_mode is not None:
            d[InputUri.SRT_MODE] = self.srt_mode
        if self.srt_key is not None:
            d[InputUri.SRT_KEY] = self.srt_key.to_dict()
        if self.rtmp_src_type is not None:
            d[InputUri.RTMP_SRT_TYPE] = self.rtmp_src_type
        if self.ndi is not None:
            d[InputUri.NDI] = self.ndi.to_dict()
        if self.aws is not None:
            d[InputUri.AWS] = self.aws.to_dict()
        if self.web_rtc is not None:
            d[InputUri.WEB_RTC] = self.web_rtc.to_dict()
        if self.programme is not None:
            d[InputUri.PROGRAMME] = self.programme.to_dict()

        return d


class GoogleProp:
    ACCOUNT_CREDS = "account_creds"

    def __init__(self, account_creds: str) -> None:
        self.account_creds = account_creds

    def to_dict(self) -> dict:
        d = dict()

        d[GoogleProp.ACCOUNT_CREDS] = self.account_creds

        return d


class AzureProp:
    ACCOUNT_NAME = "account_name"
    ACCOUNT_KEY = "account_key"

    def __init__(self, account_name: str, account_key: str) -> None:
        self.account_name = account_name
        self.account_key = account_key

    def to_dict(self) -> dict:
        d = dict()

        d[AzureProp.ACCOUNT_NAME] = self.account_name
        d[AzureProp.ACCOUNT_KEY] = self.account_key

        return d


class KvsProp:
    STREAM_NAME = "stream_name"
    ACCESS_KEY = "access_key"

    def __init__(self, stream_name: str, access_key: str) -> None:
        self.stream_name = stream_name
        self.access_key = access_key

    def to_dict(self) -> dict:
        d = dict()

        d[KvsProp.STREAM_NAME] = self.stream_name
        d[KvsProp.ACCESS_KEY] = self.access_key

        return d


class OutputUri:
    ID = "id"
    URI = "uri"

    HTTP_ROOT = "http_root"
    CHUNK_DURATION = "chunk_duration"
    HLS_TYPE = "hls_type"
    HLS_SINK_TYPE = "hlssink_type"
    TOKEN = "token"
    WHIP = "whip"
    PLAYLIST_ROOT = "playlist_root"

    SRT_MODE = "srt_mode"
    SRT_KEY = "srt_key"

    RTMP_TYPE = "rtmp_type"
    RTMP_WEB_URL = "rtmp_web_url"
    RTMP_SINK_TYPE = "rtmpsink_type"

    KVS = "kvs"
    AZURE = "azure"
    AWS = "aws"
    GOOGLE = "google"
    NDI = "ndi"

    WEB_RTC = "webrtc"
    MULTICAST_IFACE = "multicast_iface"

    def __init__(
        self,
        id: int,
        uri: str,
        http_root: Optional[str] = None,
        chunk_duration: Optional[int] = None,
        hls_type: Optional[HlsType] = None,
        hlssink_type: Optional[HlsSinkType] = None,
        srt_mode: Optional[SrtMode] = None,
        srt_key: Optional[SrtKey] = None,
        playlist_root: Optional[str] = None,
        rtmp_type: Optional[int] = None,
        rtmp_web_url: Optional[str] = None,
        rtmpsink_type: Optional[RtmpSinkType] = None,
        kvs: Optional[KvsProp] = None,
        azure: Optional[AzureProp] = None,
        aws: Optional[S3Prop] = None,
        google: Optional[GoogleProp] = None,
        ndi: Optional[NdiProp] = None,
        web_rtc: Optional[WebRTCProp] = None,
        whip: Optional[WhipProp] = None,
        token: Optional[bool] = None,
        multicast_iface: Optional[str] = None,
    ):
        self.id = id
        self.uri = uri
        self.http_root = http_root
        self.chunk_duration = chunk_duration
        self.hls_type = hls_type
        self.hlssink_type = hlssink_type
        self.srt_mode = srt_mode
        self.srt_key = srt_key
        self.playlist_root = playlist_root
        self.rtmp_type = rtmp_type
        self.rtmp_web_url = rtmp_web_url
        self.rtmpsink_type = rtmpsink_type
        self.kvs = kvs
        self.azure = azure
        self.aws = aws
        self.google = google
        self.ndi = ndi
        self.web_rtc = web_rtc
        self.whip = whip
        self.token = token
        self.multicast_iface = multicast_iface

    def to_dict(self) -> dict:
        d = dict()

        d[OutputUri.ID] = self.id
        d[OutputUri.URI] = self.uri
        if self.http_root is not None:
            d[OutputUri.HTTP_ROOT] = self.http_root
        if self.chunk_duration is not None:
            d[OutputUri.CHUNK_DURATION] = self.chunk_duration
        if self.hls_type is not None:
            d[OutputUri.HLS_TYPE] = self.hls_type
        if self.hlssink_type is not None:
            d[OutputUri.HLS_SINK_TYPE] = self.hlssink_type
        if self.srt_mode is not None:
            d[OutputUri.SRT_MODE] = self.srt_mode
        if self.srt_key is not None:
            d[OutputUri.SRT_KEY] = self.srt_key.to_dict()
        if self.playlist_root is not None:
            d[OutputUri.RTMP_TYPE] = self.rtmp_type
        if self.rtmp_web_url is not None:
            d[OutputUri.RTMP_WEB_URL] = self.rtmp_web_url
        if self.rtmpsink_type is not None:
            d[OutputUri.RTMP_SINK_TYPE] = self.rtmpsink_type
        if self.kvs is not None:
            d[OutputUri.KVS] = self.kvs.to_dict()
        if self.azure is not None:
            d[OutputUri.AZURE] = self.azure.to_dict()
        if self.google is not None:
            d[OutputUri.GOOGLE] = self.google.to_dict()
        if self.aws is not None:
            d[OutputUri.AWS] = self.aws.to_dict()
        if self.ndi is not None:
            d[OutputUri.NDI] = self.ndi.to_dict()
        if self.web_rtc is not None:
            d[OutputUri.WEB_RTC] = self.web_rtc.to_dict()
        if self.whip is not None:
            d[OutputUri.WHIP] = self.whip.to_dict()
        if self.token is not None:
            d[OutputUri.TOKEN] = self.token
        if self.multicast_iface is not None:
            d[OutputUri.MULTICAST_IFACE] = self.multicast_iface

        return d


class StreamTTL:
    TTL = "ttl"
    PHOENIX = "phoenix"

    def __init__(self, ttl: int, phoenix: bool) -> None:
        self.ttl = ttl
        self.phoenix = phoenix

    def to_dict(self) -> dict:
        d = dict()

        d[StreamTTL.TTL] = self.ttl
        d[StreamTTL.PHOENIX] = self.phoenix

        return d


class BackgroundEffect:
    TYPE = "type"
    STRENGTH = "strength"
    IMAGE = "image"

    def __init__(
        self,
        type: BackgroundEffectType,
        strength: Optional[float] = None,
        image: Optional[str] = None,
    ) -> None:
        self.type = str(type)
        self.strength = strength
        self.image = image

    def to_dict(self) -> dict:
        d = dict()

        d[BackgroundEffect.TYPE] = self.type
        if self.strength is not None:
            d[BackgroundEffect.STRENGTH] = self.strength
        if self.image is not None:
            d[BackgroundEffect.IMAGE] = self.image

        return d


class Point:
    X = "x"
    Y = "y"

    def __init__(self, x: int, y: int) -> None:
        self.x = x
        self.y = y

    def to_dict(self) -> dict:
        d = dict()

        d[Point.X] = self.x
        d[Point.Y] = self.y

        return d


class Logo:
    PATH = "path"
    POSITION = "position"

    def __init__(self, path: str, position: Point) -> None:
        self.path = path
        self.position = position

    def to_dict(self) -> dict:
        d = dict()

        d[Logo.PATH] = self.path
        d[Logo.POSITION] = self.position.to_dict()

        return d


class Size:
    WIDTH = "width"
    HEIGHT = "height"

    def __init__(self, width: int, height: int) -> None:
        self.width = width
        self.height = height

    def to_dict(self) -> dict:
        d = dict()

        d[Size.WIDTH] = self.width
        d[Size.HEIGHT] = self.height

        return d


class RSVGLogo:
    PATH = "path"
    POSITION = "position"

    def __init__(self, path: str, position: Point) -> None:
        self.path = path
        self.position = position

    def to_dict(self) -> dict:
        d = dict()

        d[RSVGLogo.PATH] = self.path
        d[RSVGLogo.POSITION] = self.position.to_dict()

        return d


class OnlineUsers:
    DAEMON = "daemon"
    HTTP = "http"
    VODS = "vods"
    CODS = "cods"

    def __init__(self, daemon: int, http: int, vods: int, cods: int) -> None:
        self.daemon = daemon
        self.http = http
        self.vods = vods
        self.cods = cods

    def to_dict(self) -> dict:
        d = dict()

        d[OnlineUsers.DAEMON] = self.daemon
        d[OnlineUsers.HTTP] = self.http
        d[OnlineUsers.VODS] = self.vods
        d[OnlineUsers.CODS] = self.cods

        return d


class ServiceStatisticInfo:
    CPU = "cpu"
    GPU = "gpu"
    LOAD_AVERAGE = "load_average"
    MEMORY_TOTAL = "memory_total"
    MEMORY_FREE = "memory_free"
    HDD_TOTAL = "hdd_total"
    HDD_FREE = "hdd_free"
    BANDWIDTH_IN = "bandwidth_in"
    BANDWIDTH_OUT = "bandwidth_out"
    UPTIME = "uptime"
    TIMESTAMP = "timestamp"
    TOTAL_BYTES_IN = "total_bytes_in"
    TOTAL_BYTES_OUT = "total_bytes_out"
    ONLINE_USERS = "online_users"

    def __init__(
        self,
        cpu: float,
        gpu: float,
        load_average: str,
        memory_total: int,
        memory_free: int,
        hdd_total: int,
        hdd_free: int,
        bandwidth_in: int,
        bandwidth_out: int,
        uptime: int,
        timestamp: int,
        total_bytes_in: int,
        total_bytes_out: int,
        online_users: OnlineUsers,
    ):
        self.cpu = cpu
        self.gpu = gpu
        self.load_average = load_average
        self.memory_total = memory_total
        self.memory_free = memory_free
        self.hdd_total = hdd_total
        self.hdd_free = hdd_free
        self.bandwidth_in = bandwidth_in
        self.bandwidth_out = bandwidth_out
        self.uptime = uptime
        self.timestamp = timestamp
        self.total_bytes_in = total_bytes_in
        self.total_bytes_out = total_bytes_out
        self.online_users = online_users

    def to_dict(self) -> dict:
        d = dict()

        d[ServiceStatisticInfo.CPU] = self.cpu
        d[ServiceStatisticInfo.GPU] = self.gpu
        d[ServiceStatisticInfo.LOAD_AVERAGE] = self.load_average
        d[ServiceStatisticInfo.MEMORY_TOTAL] = self.memory_total
        d[ServiceStatisticInfo.MEMORY_FREE] = self.memory_free
        d[ServiceStatisticInfo.HDD_TOTAL] = self.hdd_total
        d[ServiceStatisticInfo.HDD_FREE] = self.hdd_free
        d[ServiceStatisticInfo.BANDWIDTH_IN] = self.bandwidth_in
        d[ServiceStatisticInfo.BANDWIDTH_OUT] = self.bandwidth_out
        d[ServiceStatisticInfo.UPTIME] = self.uptime
        d[ServiceStatisticInfo.TIMESTAMP] = self.timestamp
        d[ServiceStatisticInfo.TOTAL_BYTES_IN] = self.total_bytes_in
        d[ServiceStatisticInfo.TOTAL_BYTES_OUT] = self.total_bytes_out
        d[ServiceStatisticInfo.ONLINE_USERS] = self.online_users.to_dict()

        return d


class QuitStatusInfo:
    ID = "id"
    EXIT_STATUS = "exit_status"

    def __init__(self, id: StreamId, exit_status: ExitStatus) -> None:
        self.id = id
        self.exit_status = exit_status

    def to_dict(self) -> dict:
        d = dict()

        d[QuitStatusInfo.ID] = self.id
        d[QuitStatusInfo.EXIT_STATUS] = self.exit_status

        return d


class InputStreamStatisticInfo:
    ID = "id"
    LAST_UPDATE_TIME = "last_update_time"

    def __init__(self, id: int, last_update_time: int) -> None:
        self.id = id
        self.last_update_time = last_update_time

    def to_dict(self) -> dict:
        d = dict()

        d[InputStreamStatisticInfo.ID] = self.id
        d[InputStreamStatisticInfo.LAST_UPDATE_TIME] = self.last_update_time

        return d


class OutputStreamStatisticInfo:
    ID = "id"
    LAST_UPDATE_TIME = "last_update_time"

    def __init__(self, id: int, last_update_time: int) -> None:
        self.id = id
        self.last_update_time = last_update_time

    def to_dict(self) -> dict:
        d = dict()

        d[OutputStreamStatisticInfo.ID] = self.id
        d[OutputStreamStatisticInfo.LAST_UPDATE_TIME] = self.last_update_time

        return d


class StreamStatisticInfo:
    ID = "id"
    TYPE = "type"
    CPU = "cpu"
    LOOP_START_TIME = "loop_start_time"
    RSS = "rss"
    STATUS = "status"
    RESTARTS = "restarts"
    START_TIME = "start_time"
    TIMESTAMP = "timestamp"
    IDLE_TIME = "idle_time"
    INPUT_STREAMS = "input_streams"
    OUTPUT_STREAMS = "output_streams"

    def __init__(
        self,
        id: StreamId,
        type: StreamType,
        cpu: float,
        loop_start_time: int,
        rss: int,
        status: StreamStatus,
        restarts: int,
        start_time: int,
        timestamp: int,
        idle_time: int,
        input_streams: List[InputStreamStatisticInfo],
        output_streams: List[OutputStreamStatisticInfo],
    ) -> None:
        self.id = id
        self.type = type
        self.cpu = cpu
        self.loop_start_time = loop_start_time
        self.rss = rss
        self.status = status
        self.restarts = restarts
        self.start_time = start_time
        self.timestamp = timestamp
        self.idle_time = idle_time
        self.input_streams = input_streams
        self.output_streams = output_streams

    def to_dict(self) -> dict:
        d = dict()

        d[StreamStatisticInfo.ID] = self.id
        d[StreamStatisticInfo.TYPE] = self.type
        d[StreamStatisticInfo.CPU] = self.cpu
        d[StreamStatisticInfo.LOOP_START_TIME] = self.loop_start_time
        d[StreamStatisticInfo.RSS] = self.rss
        d[StreamStatisticInfo.STATUS] = self.status
        d[StreamStatisticInfo.RESTARTS] = self.restarts
        d[StreamStatisticInfo.START_TIME] = self.start_time
        d[StreamStatisticInfo.TIMESTAMP] = self.timestamp
        d[StreamStatisticInfo.IDLE_TIME] = self.idle_time
        d[StreamStatisticInfo.INPUT_STREAMS] = [
            inp.to_dict() for inp in self.input_streams
        ]
        d[StreamStatisticInfo.OUTPUT_STREAMS] = [
            out.to_dict() for out in self.output_streams
        ]

        return d
