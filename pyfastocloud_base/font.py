from pyfastogt.maker import Maker


class Font(Maker):
    FONT_FAMILY_FIELD = "family"
    FONT_SIZE_FIELD = "size"

    def __init__(self):
        self.style = "Arial"
        self.size = 18

    @classmethod
    def make_entry(cls, json: dict) -> "Font":
        cl = cls()
        cl.update_entry(json)
        return cl

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, style = self.check_required_type(Font.FONT_FAMILY_FIELD, str, json)
        if res:
            self.style = style

        res, size = self.check_required_type(Font.FONT_SIZE_FIELD, float, json)
        if res:
            self.size = size

    def to_dict(self) -> dict:
        return {Font.FONT_FAMILY_FIELD: self.style, Font.FONT_SIZE_FIELD: self.size}
