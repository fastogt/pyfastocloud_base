from pyfastocloud_base.constants import AudioStabilizationType, GpuModel
from pyfastocloud_base.font import Font
from pyfastogt.maker import Maker


class AudioStabilization(Maker):
    TYPE_FIELD = "type"
    GPU_MODEL_FIELD = "gpu_model"

    def __init__(self):
        self.type = AudioStabilizationType.DENOISER
        self.gpu_model = GpuModel.NVIDIA_T4

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, tp = self.check_required_type(AudioStabilization.TYPE_FIELD, int, json)
        if res:
            self.type = tp

        res, gpu = self.check_required_type(
            AudioStabilization.GPU_MODEL_FIELD, int, json
        )
        if res:
            self.gpu_model = gpu

    def to_dict(self) -> dict:
        return {
            AudioStabilization.TYPE_FIELD: int(self.type),
            AudioStabilization.GPU_MODEL_FIELD: int(self.gpu_model),
        }
