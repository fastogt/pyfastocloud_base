from abc import ABC, abstractmethod

from pyfastogt.maker import Maker

from pyfastocloud_base.constants import BackgroundEffectType


class IBackGroundEffect(Maker, ABC):
    TYPE_FIELD = "type"

    @abstractmethod
    def type(self) -> BackgroundEffectType:
        pass

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

    def to_dict(self) -> dict:
        return {IBackGroundEffect.TYPE_FIELD: int(self.type())}


class BlurBackGroundEffect(IBackGroundEffect):
    STRENGTH_FIELD = "strength"

    def __init__(self, strength: float):
        super().__init__()
        self.strength = strength

    @abstractmethod
    def type(self) -> BackgroundEffectType:
        return BackgroundEffectType.BLUR

    def to_dict(self) -> dict:
        base = super().to_dict()
        base[BlurBackGroundEffect.STRENGTH_FIELD] = self.strength
        return base

    def update_entry(self, json: dict):
        super().update_entry(json)

        res, strength = self.check_required_type(
            BlurBackGroundEffect.STRENGTH_FIELD, float, json
        )
        if res:
            self.strength = strength


class ImageBackGroundEffect(IBackGroundEffect):
    IMAGE_FIELD = "image"

    def __init__(self, image: str):
        super().__init__()
        self.image = image

    @abstractmethod
    def type(self) -> BackgroundEffectType:
        return BackgroundEffectType.IMAGE

    def to_dict(self) -> dict:
        base = super().to_dict()
        base[ImageBackGroundEffect.IMAGE_FIELD] = self.image
        return base

    def update_entry(self, json: dict):
        super().update_entry(json)

        res, image = self.check_required_type(
            ImageBackGroundEffect.IMAGE_FIELD, str, json
        )
        if res:
            self.image = image


class ColorBackGroundEffect(IBackGroundEffect):
    COLOR_FIELD = "color"

    def __init__(self, color: int):
        super().__init__()
        self.color = color

    @abstractmethod
    def type(self) -> BackgroundEffectType:
        return BackgroundEffectType.COLOR

    def to_dict(self) -> dict:
        base = super().to_dict()
        base[ColorBackGroundEffect.COLOR_FIELD] = self.color
        return base

    def update_entry(self, json: dict):
        super().update_entry(json)

        res, color = self.check_required_type(
            ColorBackGroundEffect.COLOR_FIELD, int, json
        )
        if res:
            self.color = color
