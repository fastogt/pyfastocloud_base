from pyfastogt.maker import Maker

from pyfastocloud_base.constants import MlBackends


class MachineLearning(Maker):
    BACKEND_FIELD = "backend"
    MODEL_URL_FIELD = "model_url"
    TRACKING_FIELD = "tracking"
    DUMP_FIELD = "dump"
    CLASS_ID_FIELD = "class_id"
    OVERLAY_FIELD = "overlay"

    def __init__(self):
        self.backend = MlBackends.NVIDIA
        self.model_url = str()
        self.tracking = False
        self.dump = False
        self.class_id = 0
        self.overlay = False

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, backend = self.check_required_type(
            MachineLearning.BACKEND_FIELD, int, json
        )
        if res:
            self.backend = MlBackends(backend)

        res, model_url = self.check_required_type(
            MachineLearning.MODEL_URL_FIELD, str, json
        )
        if res:
            self.model_url = model_url

        res, tracking = self.check_required_type(
            MachineLearning.TRACKING_FIELD, bool, json
        )
        if res:
            self.tracking = tracking

        res, dump = self.check_required_type(MachineLearning.DUMP_FIELD, bool, json)
        if res:
            self.dump = dump

        res, class_id = self.check_required_type(
            MachineLearning.CLASS_ID_FIELD, int, json
        )
        if res:
            self.class_id = class_id

        res, overlay = self.check_required_type(
            MachineLearning.OVERLAY_FIELD, bool, json
        )
        if res:
            self.overlay = overlay

    def to_dict(self) -> dict:
        return {
            MachineLearning.BACKEND_FIELD: int(self.backend),
            MachineLearning.MODEL_URL_FIELD: self.model_url,
            MachineLearning.TRACKING_FIELD: self.tracking,
            MachineLearning.DUMP_FIELD: self.dump,
            MachineLearning.CLASS_ID_FIELD: self.class_id,
            MachineLearning.OVERLAY_FIELD: self.overlay,
        }
