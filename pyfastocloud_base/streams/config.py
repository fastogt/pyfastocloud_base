from abc import ABC, abstractmethod
from typing import Any, List, Optional, Dict
from fractions import Fraction

from pyfastocloud_base.ml.audio_stabilization import AudioStabilization
from pyfastocloud_base.constants import (
    AudioCodec,
    BackgroundEffect,
    BitRate,
    InputUri,
    Logo,
    OutputUri,
    RSVGLogo,
    Size,
    StreamTTL,
    StreamType,
    StreamLogLevel,
    StreamId,
    VideoCodec,
    VideoFlip,
    VideoParser,
    AudioParser,
    Volume,
)
from pyfastogt.maker import Maker
from pyfastocloud_base.ml.machine_learning import MachineLearning

from pyfastocloud_base.stream_overlay import StreamOverlay
from pyfastocloud_base.text_overlay import TextOverlay


class IStreamConfig(Maker, ABC):
    ID_FIELD = "id"
    TYPE_FIELD = "type"
    OUTPUT_FIELD = "output"

    def __init__(self, sid: StreamId, output: List[OutputUri]):
        self.id = sid
        self.output = output

    def is_valid(self) -> bool:
        if self.id:
            return True
        return False

    def to_dict(self) -> dict:
        return {
            IStreamConfig.ID_FIELD: self.id,  # required
            IStreamConfig.TYPE_FIELD: int(self.type()),  # required
            IStreamConfig.OUTPUT_FIELD: [
                out.to_dict() for out in self.output
            ],  # required empty in timeshift_record
        }

    @abstractmethod
    def type(self) -> StreamType:
        pass

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, sid = self.check_required_type(IStreamConfig.ID_FIELD, str, json)
        if res:
            self.id = sid

        res, tp = self.check_required_type(IStreamConfig.TYPE_FIELD, int, json)
        if res:
            self.type = StreamType(tp)

        res, output = self.check_required_type(IStreamConfig.OUTPUT_FIELD, list, json)
        if res:
            self.output = output


class ProxyConfig(IStreamConfig):
    def __init__(self, sid: StreamId, output: List[OutputUri]):
        super().__init__(sid=sid, output=output)

    def type(self) -> StreamType:
        return StreamType.PROXY


class HardwareConfig(IStreamConfig):
    FEEDBACK_DIR_FIELD = "feedback_directory"
    DATA_DIR_FIELD = "data_directory"
    LOG_LEVEL_FIELD = "log_level"
    LOOP_FIELD = "loop"
    SELECTED_INPUT_FIELD = "selected_input"
    HAVE_VIDEO_FIELD = "have_video"
    HAVE_AUDIO_FIELD = "have_audio"
    RESTART_ATTEMPTS_FIELD = "restart_attempts"
    AUTO_EXIT_TIME_FIELD = "auto_exit_time"
    INPUT_FIELD = "input"
    AUDIO_SELECT_FIELD = "audio_select"
    AUDIO_TRACKS_COUNT_FIELD = "audio_tracks_count"
    EXTRA_CONFIG = "extra_config"

    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
    ):
        super().__init__(sid=sid, output=output)
        self.feedback_dir = feedback_dir
        self.data_directory = data_directory
        self.log_level = log_level
        self.loop = loop
        self.selected_input = selected_input
        self.have_video = have_video
        self.have_audio = have_audio
        self.restart_attempts = restart_attempts
        self.audio_tracks_count = audio_tracks_count
        self.input = input
        self.audio_select = audio_select
        self.auto_exit_time = auto_exit_time
        self.extra_config = extra_config

    def is_valid(self) -> bool:
        return super().is_valid() and self.feedback_dir is not None

    def to_dict(self) -> dict:
        base = super().to_dict()
        base[HardwareConfig.FEEDBACK_DIR_FIELD] = self.feedback_dir
        base[HardwareConfig.DATA_DIR_FIELD] = self.data_directory
        base[HardwareConfig.LOG_LEVEL_FIELD] = int(self.log_level)
        base[HardwareConfig.LOOP_FIELD] = self.loop
        base[HardwareConfig.SELECTED_INPUT_FIELD] = self.selected_input
        base[HardwareConfig.HAVE_VIDEO_FIELD] = self.have_video
        base[HardwareConfig.HAVE_AUDIO_FIELD] = self.have_audio
        base[HardwareConfig.RESTART_ATTEMPTS_FIELD] = self.restart_attempts
        base[HardwareConfig.AUDIO_TRACKS_COUNT_FIELD] = self.audio_tracks_count
        base[HardwareConfig.INPUT_FIELD] = [inp.to_dict() for inp in self.input]
        if self.audio_select is not None:
            base[HardwareConfig.AUDIO_SELECT_FIELD] = self.audio_select
        if self.auto_exit_time is not None:
            base[HardwareConfig.AUTO_EXIT_TIME_FIELD] = self.auto_exit_time.to_dict()
        if self.extra_config is not None:
            base[HardwareConfig.EXTRA_CONFIG] = self.extra_config

        return base

    def update_entry(self, json: dict):
        super().update_entry(json)

        res, feedback_dir = self.check_required_type(
            HardwareConfig.FEEDBACK_DIR_FIELD, str, json
        )
        if res:
            self.feedback_dir = feedback_dir

        res, data_directory = self.check_required_type(
            HardwareConfig.DATA_DIR_FIELD, str, json
        )
        if res:
            self.data_directory = data_directory

        res, log_level = self.check_required_type(
            HardwareConfig.LOG_LEVEL_FIELD, int, json
        )
        if res:
            self.log_level = log_level

        res, loop = self.check_required_type(HardwareConfig.LOOP_FIELD, bool, json)
        if res:
            self.loop = loop

        res, selected = self.check_required_type(
            HardwareConfig.SELECTED_INPUT_FIELD, int, json
        )
        if res:
            self.selected_input = selected

        res, have_video = self.check_required_type(
            HardwareConfig.HAVE_VIDEO_FIELD, bool, json
        )
        if res:
            self.have_video = have_video

        res, have_audio = self.check_required_type(
            HardwareConfig.HAVE_AUDIO_FIELD, bool, json
        )
        if res:
            self.have_audio = have_audio

        res, restart_attempts = self.check_required_type(
            HardwareConfig.RESTART_ATTEMPTS_FIELD, int, json
        )
        if res:
            self.restart_attempts = restart_attempts

        res, audio_tracks_count = self.check_required_type(
            HardwareConfig.AUDIO_TRACKS_COUNT_FIELD, int, json
        )
        if res:
            self.audio_tracks_count = audio_tracks_count

        res, inp = self.check_required_type(HardwareConfig.INPUT_FIELD, dict, json)
        if res:
            self.input = inp

        res, audio_select = self.check_optional_type(
            HardwareConfig.AUDIO_SELECT_FIELD, int, json
        )
        if res:
            self.audio_select = audio_select
        else:
            self.audio_select = None

        res, auto_exit_time = self.check_optional_type(
            HardwareConfig.AUTO_EXIT_TIME_FIELD, dict, json
        )
        if res:
            self.auto_exit_time = auto_exit_time
        else:
            self.auto_exit_time = None

        res, extra_config = self.check_optional_type(
            HardwareConfig.EXTRA_CONFIG, dict, json
        )
        if res:
            self.extra_config = extra_config
        else:
            self.extra_config = None


class RelayConfig(HardwareConfig):
    VIDEO_PARSER_FIELD = "video_parser"
    AUDIO_PARSER_FIELD = "audio_parser"

    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        video_parser: Optional[VideoParser] = None,
        audio_parser: Optional[AudioParser] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
        )
        self.video_parser = video_parser
        self.audio_parser = audio_parser

    def type(self) -> StreamType:
        return StreamType.RELAY

    def is_valid(self) -> bool:
        return super().is_valid()

    def to_dict(self) -> dict:
        base = super().to_dict()
        if self.video_parser is not None:
            base[RelayConfig.VIDEO_PARSER_FIELD] = self.video_parser
        if self.audio_parser is not None:
            base[RelayConfig.AUDIO_PARSER_FIELD] = self.audio_parser

        return base

    def update_entry(self, json: dict):
        super().update_entry(json)
        res, video_parser = self.check_optional_type(
            RelayConfig.VIDEO_PARSER_FIELD, str, json
        )
        if res:
            self.video_parser = video_parser
        else:
            self.video_parser = None

        res, audio_parser = self.check_optional_type(
            RelayConfig.AUDIO_PARSER_FIELD, str, json
        )
        if res:
            self.audio_parser = audio_parser
        else:
            self.audio_parser = None


class EncodeConfig(HardwareConfig):
    RELAY_AUDIO_FIELD = "relay_audio"
    RELAY_VIDEO_FIELD = "relay_video"
    DEINTERLACE_FIELD = "deinterlace"
    VIDEOFLIP_FIELD = "video_flip"
    RESAMPLE_FIELD = "resample"
    VOLUME_FIELD = "volume"
    VIDEO_CODEC_FIELD = "video_codec"
    AUDIO_CODEC_FIELD = "audio_codec"
    # optional
    FRAME_RATE_FIELD = "frame_rate"
    AUDIO_CHANNELS_COUNT_FIELD = "audio_channels_count"
    SIZE_FIELD = "size"
    MACHINE_LEARNING_FIELD = "machine_learning"
    VIDEO_BITRATE_FIELD = "video_bitrate"
    AUDIO_BITRATE_FIELD = "audio_bitrate"
    LOGO_FIELD = "logo"
    RSVG_LOGO_FIELD = "rsvg_logo"
    ASPECT_RATIO_FIELD = "aspect_ratio"
    BACKGROUND_EFFECT_FILED = "background_effect"
    TEXT_OVERLAY_FIELD = "text_overlay"
    STREAM_OVERLAY_FIELD = "stream_overlay"
    AUDIO_STABILIZATION_FIELD = "audio_stabilization"

    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        relay_video: bool,
        relay_audio: bool,
        deinterlace: bool,
        resample: bool,
        volume: Volume,
        video_codec: VideoCodec,
        audio_codec: AudioCodec,
        audio_select=None,
        auto_exit_time=None,
        extra_config: Optional[Dict[str, Any]] = None,
        frame_rate: Optional[Fraction] = None,
        audio_channels_count: Optional[int] = None,
        size: Optional[Size] = None,
        machine_learning: Optional[MachineLearning] = None,
        video_bit_rate: Optional[BitRate] = None,
        audio_bit_rate: Optional[BitRate] = None,
        logo: Optional[Logo] = None,
        rsvg_logo: Optional[RSVGLogo] = None,
        aspect_ratio: Optional[Fraction] = None,
        background_effect: Optional[BackgroundEffect] = None,
        text_overlay: Optional[TextOverlay] = None,
        video_flip: Optional[VideoFlip] = None,
        stream_overlay: Optional[StreamOverlay] = None,
        audio_stabilization: Optional[AudioStabilization] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
        )
        self.relay_audio = relay_audio
        self.relay_video = relay_video
        self.deinterlace = deinterlace
        self.resample = resample
        self.volume = volume
        self.video_codec = video_codec
        self.audio_codec = audio_codec
        self.frame_rate = frame_rate
        self.audio_channels_count = audio_channels_count
        self.size = size
        self.machine_learning = machine_learning
        self.video_bit_rate = video_bit_rate
        self.audio_bit_rate = audio_bit_rate
        self.logo = logo
        self.rsvg_logo = rsvg_logo
        self.aspect_ratio = aspect_ratio
        self.background_effect = background_effect
        self.text_overlay = text_overlay
        self.video_flip = video_flip
        self.stream_overlay = stream_overlay
        self.audio_stabilization = audio_stabilization

    def type(self) -> StreamType:
        return StreamType.ENCODE

    def is_valid(self) -> bool:
        return super().is_valid()

    def to_dict(self) -> dict:
        base = super().to_dict()
        base[EncodeConfig.RELAY_AUDIO_FIELD] = self.relay_audio
        base[EncodeConfig.RELAY_VIDEO_FIELD] = self.relay_video
        base[EncodeConfig.VIDEO_CODEC_FIELD] = self.video_codec
        base[EncodeConfig.AUDIO_CODEC_FIELD] = self.audio_codec

        if self.resample is not None:
            base[EncodeConfig.RESAMPLE_FIELD] = self.resample

        if self.deinterlace is not None:
            base[EncodeConfig.DEINTERLACE_FIELD] = self.deinterlace

        if self.volume is not None:
            base[EncodeConfig.VOLUME_FIELD] = self.volume
        if self.frame_rate is not None:
            base[EncodeConfig.FRAME_RATE_FIELD] = self.frame_rate

        if self.audio_channels_count is not None:
            base[EncodeConfig.AUDIO_CHANNELS_COUNT_FIELD] = self.audio_channels_count

        if self.size is not None:
            base[EncodeConfig.SIZE_FIELD] = self.size

        if self.machine_learning is not None:
            base[EncodeConfig.MACHINE_LEARNING_FIELD] = self.machine_learning

        if self.video_bit_rate is not None:
            base[EncodeConfig.VIDEO_BITRATE_FIELD] = self.video_bit_rate

        if self.audio_bit_rate is not None:
            base[EncodeConfig.AUDIO_BITRATE_FIELD] = self.audio_bit_rate

        if self.logo is not None:
            base[EncodeConfig.LOGO_FIELD] = self.logo

        if self.rsvg_logo is not None:
            base[EncodeConfig.RSVG_LOGO_FIELD] = self.rsvg_logo

        if self.aspect_ratio is not None:
            base[EncodeConfig.ASPECT_RATIO_FIELD] = self.aspect_ratio

        if self.background_effect is not None:
            base[EncodeConfig.BACKGROUND_EFFECT_FILED] = self.background_effect

        if self.text_overlay is not None:
            base[EncodeConfig.TEXT_OVERLAY_FIELD] = self.text_overlay

        if self.video_flip is not None:
            base[EncodeConfig.VIDEOFLIP_FIELD] = self.video_flip

        if self.stream_overlay is not None:
            base[EncodeConfig.STREAM_OVERLAY_FIELD] = self.stream_overlay

        if self.audio_stabilization is not None:
            base[EncodeConfig.AUDIO_STABILIZATION_FIELD] = self.audio_stabilization

        return base

    def update_entry(self, json: dict):
        super().update_entry(json)

        res, video_codec = self.check_required_type(
            EncodeConfig.VIDEO_CODEC_FIELD, str, json
        )
        if res:
            self.video_codec = video_codec

        res, audio_codec = self.check_required_type(
            EncodeConfig.AUDIO_CODEC_FIELD, str, json
        )
        if res:
            self.audio_codec = audio_codec

        res, deinterlace = self.check_optional_type(
            EncodeConfig.DEINTERLACE_FIELD, bool, json
        )
        if res:
            self.deinterlace = deinterlace
        else:
            self.deinterlace = None

        res, resample = self.check_optional_type(
            EncodeConfig.RESAMPLE_FIELD, bool, json
        )
        if res:
            self.resample = resample
        else:
            self.resample = None

        res, volume = self.check_optional_type(EncodeConfig.VOLUME_FIELD, float, json)
        if res:
            self.volume = volume
        else:
            self.volume = None

        res, frame_rate = self.check_optional_type(
            EncodeConfig.FRAME_RATE_FIELD, dict, json
        )
        if res:
            self.frame_rate = frame_rate
        else:
            self.frame_rate = None

        res, audio_channels_count = self.check_optional_type(
            EncodeConfig.AUDIO_CHANNELS_COUNT_FIELD, int, json
        )
        if res:
            self.audio_channels_count = audio_channels_count
        else:
            self.audio_channels_count = None

        res, size = self.check_optional_type(EncodeConfig.SIZE_FIELD, dict, json)
        if res:
            self.size = size
        else:
            self.size = None

        res, machine_learning = self.check_optional_type(
            EncodeConfig.MACHINE_LEARNING_FIELD, dict, json
        )
        if res:
            self.machine_learning = machine_learning
        else:
            self.machine_learning = None

        res, video_bit_rate = self.check_optional_type(
            EncodeConfig.VIDEO_BITRATE_FIELD, int, json
        )
        if res:
            self.video_bit_rate = video_bit_rate
        else:
            self.video_bit_rate = None

        res, audio_bit_rate = self.check_optional_type(
            EncodeConfig.AUDIO_BITRATE_FIELD, int, json
        )
        if res:
            self.audio_bit_rate = audio_bit_rate
        else:
            self.audio_bit_rate = None

        res, logo = self.check_optional_type(EncodeConfig.LOGO_FIELD, dict, json)
        if res:
            self.logo = logo
        else:
            self.logo = None

        res, rsvg_logo = self.check_optional_type(
            EncodeConfig.RSVG_LOGO_FIELD, dict, json
        )
        if res:
            self.rsvg_logo = rsvg_logo
        else:
            self.rsvg_logo = None

        res, aspect_ratio = self.check_optional_type(
            EncodeConfig.ASPECT_RATIO_FIELD, dict, json
        )
        if res:
            self.aspect_ratio = aspect_ratio
        else:
            self.aspect_ratio = None

        res, background_effect = self.check_optional_type(
            EncodeConfig.BACKGROUND_EFFECT_FILED, dict, json
        )
        if res:
            self.background_effect = background_effect
        else:
            self.background_effect = None

        res, text_overlay = self.check_optional_type(
            EncodeConfig.TEXT_OVERLAY_FIELD, dict, json
        )
        if res:
            self.text_overlay = text_overlay
        else:
            self.text_overlay = None

        res, video_flip = self.check_optional_type(
            EncodeConfig.VIDEOFLIP_FIELD, int, json
        )
        if res:
            self.video_flip = video_flip
        else:
            self.video_flip = None

        res, stream_overlay = self.check_optional_type(
            EncodeConfig.STREAM_OVERLAY_FIELD, str, json
        )
        if res:
            self.stream_overlay = stream_overlay
        else:
            self.stream_overlay = None

        res, audio_stab = self.check_optional_type(
            EncodeConfig.AUDIO_STABILIZATION_FIELD, dict, json
        )
        if res:
            self.audio_stabilization = audio_stab
        else:
            self.audio_stabilization = None


class TimeShiftRecordConfig(RelayConfig):
    TIMESHIFT_CHUNK_DURATION = "timeshift_chunk_duration"
    TIMESHIFT_CHUNK_LIFE_TIME = "timeshift_chunk_life_time"
    TIMESHIFT_DIR = "timeshift_dir"

    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        timeshift_chunk_duration: int,
        timeshift_chunk_life_time: int,
        timeshift_dir: str,
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        video_parser: Optional[VideoParser] = None,
        audio_parser: Optional[AudioParser] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            audio_parser=audio_parser,
            video_parser=video_parser,
        )
        self.timeshift_chunk_duration = timeshift_chunk_duration
        self.timeshift_chunk_life_time = timeshift_chunk_life_time
        self.timeshift_dir = timeshift_dir

    def type(self) -> StreamType:
        return StreamType.TIMESHIFT_RECORDER

    def is_valid(self) -> bool:
        return super().is_valid()

    def to_dict(self) -> dict:
        base = super().to_dict()
        base[
            TimeShiftRecordConfig.TIMESHIFT_CHUNK_DURATION
        ] = self.timeshift_chunk_duration
        base[
            TimeShiftRecordConfig.TIMESHIFT_CHUNK_LIFE_TIME
        ] = self.timeshift_chunk_life_time
        base[TimeShiftRecordConfig.TIMESHIFT_DIR] = self.timeshift_dir
        return base

    def update_entry(self, json: dict):
        super().update_entry(json)

        res, timeshift_chunk_duration = self.check_required_type(
            TimeShiftRecordConfig.TIMESHIFT_CHUNK_DURATION, int, json
        )
        if res:
            self.timeshift_chunk_duration = timeshift_chunk_duration

        res, timeshift_chunk_life_time = self.check_required_type(
            TimeShiftRecordConfig.TIMESHIFT_CHUNK_LIFE_TIME, int, json
        )
        if res:
            self.timeshift_chunk_life_time = timeshift_chunk_life_time

        res, timeshift_dir = self.check_required_type(
            TimeShiftRecordConfig.TIMESHIFT_DIR, str, json
        )
        if res:
            self.timeshift_dir = timeshift_dir


class CatchupConfig(TimeShiftRecordConfig):
    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        timeshift_chunk_duration: int,
        timeshift_chunk_life_time: int,
        timeshift_dir: str,
        start: int,
        stop: int,
        audio_select: Optional[int] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        video_parser: Optional[VideoParser] = None,
        audio_parser: Optional[AudioParser] = None,
    ):
        seconds = CatchupConfig.calc_auto_exit_time_in_seconds(start, stop)
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=seconds,
            extra_config=extra_config,
            audio_parser=audio_parser,
            video_parser=video_parser,
            timeshift_chunk_duration=timeshift_chunk_duration,
            timeshift_chunk_life_time=timeshift_chunk_life_time,
            timeshift_dir=timeshift_dir,
        )

    @staticmethod
    def calc_auto_exit_time_in_seconds(start: int, stop: int) -> StreamTTL:
        diff_msec = stop - start
        seconds = int(diff_msec / 1000)
        return StreamTTL(seconds, False)

    def type(self) -> StreamType:
        return StreamType.CATCHUP

    def is_valid(self) -> bool:
        return super().is_valid()


class TimeshiftPlayerConfig(RelayConfig):
    TIMESHIFT_DIR = "timeshift_dir"
    TIMESHIFT_DELAY = "timeshift_delay"

    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        timeshift_dir: str,
        timeshift_delay: int,
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        video_parser: Optional[VideoParser] = None,
        audio_parser: Optional[AudioParser] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            audio_parser=audio_parser,
            video_parser=video_parser,
        )
        self.timeshift_dir = timeshift_dir
        self.timeshift_delay = timeshift_delay

    def type(self) -> StreamType:
        return StreamType.TIMESHIFT_PLAYER

    def is_valid(self) -> bool:
        return super().is_valid()

    def to_dict(self) -> dict:
        base = super().to_dict()
        base[TimeshiftPlayerConfig.TIMESHIFT_DIR] = self.timeshift_dir
        base[TimeshiftPlayerConfig.TIMESHIFT_DELAY] = self.timeshift_delay
        return base

    def update_entry(self, json: dict):
        super().update_entry(json)

        res, timeshift_dir = self.check_required_type(
            TimeshiftPlayerConfig.TIMESHIFT_DIR, str, json
        )
        if res:
            self.timeshift_dir = timeshift_dir

        res, timeshift_delay = self.check_required_type(
            TimeshiftPlayerConfig.TIMESHIFT_DELAY, int, json
        )
        if res:
            self.timeshift_delay = timeshift_delay


class TestLifeConfig(RelayConfig):
    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        video_parser: Optional[VideoParser] = None,
        audio_parser: Optional[AudioParser] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            audio_parser=audio_parser,
            video_parser=video_parser,
        )

    def type(self) -> StreamType:
        return StreamType.TEST_LIFE


class ChangerRelayConfig(RelayConfig):
    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        video_parser: Optional[VideoParser] = None,
        audio_parser: Optional[AudioParser] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            audio_parser=audio_parser,
            video_parser=video_parser,
        )

    def type(self) -> StreamType:
        return StreamType.CHANGER_RELAY


class CodRelayConfig(RelayConfig):
    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        video_parser: Optional[VideoParser] = None,
        audio_parser: Optional[AudioParser] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            audio_parser=audio_parser,
            video_parser=video_parser,
        )

    def type(self) -> StreamType:
        return StreamType.COD_RELAY


class CodEncodeConfig(EncodeConfig):
    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        relay_video: bool,
        relay_audio: bool,
        deinterlace: bool,
        resample: bool,
        volume: Volume,
        video_codec: VideoCodec,
        audio_codec: AudioCodec,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        frame_rate: Optional[Fraction] = None,
        audio_channels_count: Optional[int] = None,
        size: Optional[Size] = None,
        machine_learning: Optional[MachineLearning] = None,
        video_bit_rate: Optional[BitRate] = None,
        audio_bit_rate: Optional[BitRate] = None,
        logo: Optional[Logo] = None,
        rsvg_logo: Optional[RSVGLogo] = None,
        aspect_ratio: Optional[Fraction] = None,
        background_effect: Optional[BackgroundEffect] = None,
        text_overlay: Optional[TextOverlay] = None,
        video_flip: Optional[VideoFlip] = None,
        stream_overlay: Optional[StreamOverlay] = None,
        audio_stabilization: Optional[AudioStabilization] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            relay_video=relay_video,
            relay_audio=relay_audio,
            deinterlace=deinterlace,
            resample=resample,
            volume=volume,
            video_codec=video_codec,
            audio_codec=audio_codec,
            frame_rate=frame_rate,
            audio_channels_count=audio_channels_count,
            size=size,
            machine_learning=machine_learning,
            video_bit_rate=video_bit_rate,
            audio_bit_rate=audio_bit_rate,
            logo=logo,
            rsvg_logo=rsvg_logo,
            aspect_ratio=aspect_ratio,
            background_effect=background_effect,
            text_overlay=text_overlay,
            video_flip=video_flip,
            stream_overlay=stream_overlay,
            audio_stabilization=audio_stabilization,
        )

    def type(self) -> StreamType:
        return StreamType.COD_ENCODE


class ProxyVodConfig(ProxyConfig):
    def __init__(self, sid: str, output: list):
        super().__init__(sid=sid, output=output)

    def type(self) -> StreamType:
        return StreamType.VOD_PROXY


class VodRelayConfig(RelayConfig):
    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        http_proxy: Optional[str] = None,
        https_proxy: Optional[str] = None,
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        video_parser: Optional[VideoParser] = None,
        audio_parser: Optional[AudioParser] = None,
    ):
        if http_proxy is not None or https_proxy is not None:
            for url in input:
                if url.stream_link:
                    url.stream_link.http_proxy = (
                        http_proxy if http_proxy is not None else None
                    )
                    url.stream_link.https_proxy = (
                        https_proxy if https_proxy is not None else None
                    )

        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            audio_parser=audio_parser,
            video_parser=video_parser,
        )

    def type(self) -> StreamType:
        return StreamType.VOD_RELAY

    def is_valid(self) -> bool:
        return super().is_valid()

    def to_dict(self) -> dict:
        base = super().to_dict()
        return base


class VodEncodeConfig(EncodeConfig):
    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        relay_video: bool,
        relay_audio: bool,
        deinterlace: bool,
        resample: bool,
        volume: Volume,
        video_codec: VideoCodec,
        audio_codec: AudioCodec,
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        frame_rate: Optional[Fraction] = None,
        audio_channels_count: Optional[int] = None,
        size: Optional[Size] = None,
        machine_learning: Optional[MachineLearning] = None,
        video_bit_rate: Optional[BitRate] = None,
        audio_bit_rate: Optional[BitRate] = None,
        logo: Optional[Logo] = None,
        rsvg_logo: Optional[RSVGLogo] = None,
        aspect_ratio: Optional[Fraction] = None,
        background_effect: Optional[BackgroundEffect] = None,
        text_overlay: Optional[TextOverlay] = None,
        video_flip: Optional[VideoFlip] = None,
        stream_overlay: Optional[StreamOverlay] = None,
        audio_stabilization: Optional[AudioStabilization] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            relay_video=relay_video,
            relay_audio=relay_audio,
            deinterlace=deinterlace,
            resample=resample,
            volume=volume,
            video_codec=video_codec,
            audio_codec=audio_codec,
            frame_rate=frame_rate,
            audio_channels_count=audio_channels_count,
            size=size,
            machine_learning=machine_learning,
            video_bit_rate=video_bit_rate,
            audio_bit_rate=audio_bit_rate,
            logo=logo,
            rsvg_logo=rsvg_logo,
            aspect_ratio=aspect_ratio,
            background_effect=background_effect,
            text_overlay=text_overlay,
            video_flip=video_flip,
            stream_overlay=stream_overlay,
            audio_stabilization=audio_stabilization,
        )

    def type(self) -> StreamType:
        return StreamType.VOD_ENCODE

    def is_valid(self) -> bool:
        return super().is_valid()

    def to_dict(self) -> dict:
        base = super().to_dict()
        return base


class EventConfig(VodEncodeConfig):
    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        relay_video: bool,
        relay_audio: bool,
        deinterlace: bool,
        resample: bool,
        volume: Volume,
        video_codec: VideoCodec,
        audio_codec: AudioCodec,
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        frame_rate: Optional[Fraction] = None,
        audio_channels_count: Optional[int] = None,
        size: Optional[Size] = None,
        machine_learning: Optional[MachineLearning] = None,
        video_bit_rate: Optional[BitRate] = None,
        audio_bit_rate: Optional[BitRate] = None,
        logo: Optional[Logo] = None,
        rsvg_logo: Optional[RSVGLogo] = None,
        aspect_ratio: Optional[Fraction] = None,
        background_effect: Optional[BackgroundEffect] = None,
        text_overlay: Optional[TextOverlay] = None,
        video_flip: Optional[VideoFlip] = None,
        stream_overlay: Optional[StreamOverlay] = None,
        audio_stabilization: Optional[AudioStabilization] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            relay_video=relay_video,
            relay_audio=relay_audio,
            deinterlace=deinterlace,
            resample=resample,
            volume=volume,
            video_codec=video_codec,
            audio_codec=audio_codec,
            frame_rate=frame_rate,
            audio_channels_count=audio_channels_count,
            size=size,
            machine_learning=machine_learning,
            video_bit_rate=video_bit_rate,
            audio_bit_rate=audio_bit_rate,
            logo=logo,
            rsvg_logo=rsvg_logo,
            aspect_ratio=aspect_ratio,
            background_effect=background_effect,
            text_overlay=text_overlay,
            video_flip=video_flip,
            stream_overlay=stream_overlay,
            audio_stabilization=audio_stabilization,
        )

    def type(self) -> StreamType:
        return StreamType.EVENT


class CvDataConfig(EncodeConfig):
    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        relay_video: bool,
        relay_audio: bool,
        deinterlace: bool,
        resample: bool,
        volume: Volume,
        video_codec: VideoCodec,
        audio_codec: AudioCodec,
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        frame_rate: Optional[Fraction] = None,
        audio_channels_count: Optional[int] = None,
        size: Optional[Size] = None,
        machine_learning: Optional[MachineLearning] = None,
        video_bit_rate: Optional[BitRate] = None,
        audio_bit_rate: Optional[BitRate] = None,
        logo: Optional[Logo] = None,
        rsvg_logo: Optional[RSVGLogo] = None,
        aspect_ratio: Optional[Fraction] = None,
        background_effect: Optional[BackgroundEffect] = None,
        text_overlay: Optional[TextOverlay] = None,
        video_flip: Optional[VideoFlip] = None,
        stream_overlay: Optional[StreamOverlay] = None,
        audio_stabilization: Optional[AudioStabilization] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            relay_video=relay_video,
            relay_audio=relay_audio,
            deinterlace=deinterlace,
            resample=resample,
            volume=volume,
            video_codec=video_codec,
            audio_codec=audio_codec,
            frame_rate=frame_rate,
            audio_channels_count=audio_channels_count,
            size=size,
            machine_learning=machine_learning,
            video_bit_rate=video_bit_rate,
            audio_bit_rate=audio_bit_rate,
            logo=logo,
            rsvg_logo=rsvg_logo,
            aspect_ratio=aspect_ratio,
            background_effect=background_effect,
            text_overlay=text_overlay,
            video_flip=video_flip,
            stream_overlay=stream_overlay,
            audio_stabilization=audio_stabilization,
        )

    def type(self) -> StreamType:
        return StreamType.CV_DATA


class ChangerEncodeConfig(EncodeConfig):
    def __init__(
        self,
        sid: StreamId,
        output: List[OutputUri],
        feedback_dir: str,
        data_directory: str,
        log_level: StreamLogLevel,
        loop: bool,
        selected_input: int,
        have_video: bool,
        have_audio: bool,
        restart_attempts: int,
        audio_tracks_count: int,
        input: List[InputUri],
        relay_video: bool,
        relay_audio: bool,
        deinterlace: bool,
        resample: bool,
        volume: Volume,
        video_codec: VideoCodec,
        audio_codec: AudioCodec,
        audio_select: Optional[int] = None,
        auto_exit_time: Optional[StreamTTL] = None,
        extra_config: Optional[Dict[str, Any]] = None,
        frame_rate: Optional[Fraction] = None,
        audio_channels_count: Optional[int] = None,
        size: Optional[Size] = None,
        machine_learning: Optional[MachineLearning] = None,
        video_bit_rate: Optional[BitRate] = None,
        audio_bit_rate: Optional[BitRate] = None,
        logo: Optional[Logo] = None,
        rsvg_logo: Optional[RSVGLogo] = None,
        aspect_ratio: Optional[Fraction] = None,
        background_effect: Optional[BackgroundEffect] = None,
        video_flip: Optional[VideoFlip] = None,
        text_overlay: Optional[TextOverlay] = None,
        stream_overlay: Optional[StreamOverlay] = None,
        audio_stabilization: Optional[AudioStabilization] = None,
    ):
        super().__init__(
            sid=sid,
            output=output,
            feedback_dir=feedback_dir,
            data_directory=data_directory,
            log_level=log_level,
            loop=loop,
            selected_input=selected_input,
            have_video=have_video,
            have_audio=have_audio,
            restart_attempts=restart_attempts,
            audio_tracks_count=audio_tracks_count,
            input=input,
            audio_select=audio_select,
            auto_exit_time=auto_exit_time,
            extra_config=extra_config,
            relay_video=relay_video,
            relay_audio=relay_audio,
            deinterlace=deinterlace,
            resample=resample,
            volume=volume,
            video_codec=video_codec,
            audio_codec=audio_codec,
            frame_rate=frame_rate,
            audio_channels_count=audio_channels_count,
            size=size,
            machine_learning=machine_learning,
            video_bit_rate=video_bit_rate,
            audio_bit_rate=audio_bit_rate,
            logo=logo,
            rsvg_logo=rsvg_logo,
            aspect_ratio=aspect_ratio,
            background_effect=background_effect,
            text_overlay=text_overlay,
            video_flip=video_flip,
            stream_overlay=stream_overlay,
            audio_stabilization=audio_stabilization,
        )

    def type(self) -> StreamType:
        return StreamType.CHANGER_ENCODE
