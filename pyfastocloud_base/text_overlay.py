from pyfastogt.maker import Maker
from pyfastocloud_base.font import Font


class TextOverlay(Maker):
    FONT_FIELD = "font"
    TEXT_FIELD = "text"
    X_ABSOLUTE_FILED = "x_absolute"
    Y_ABSOLUTE_FILED = "y_absolute"

    def __init__(self):
        self.text = str()
        self.x_absolute = 0.5
        self.y_absolute = 0.5

        self.font = None

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, text = self.check_required_type(TextOverlay.TEXT_FIELD, str, json)
        if res:
            self.text = text

        res, xabs = self.check_required_type(TextOverlay.X_ABSOLUTE_FILED, float, json)
        if res:
            self.text = xabs

        res, yabs = self.check_required_type(TextOverlay.Y_ABSOLUTE_FILED, float, json)
        if res:
            self.yabs = yabs

        res, font = self.check_optional_type(TextOverlay.FONT_FIELD, dict, json)
        if res:
            self.font = Font.make_entry(font)
        else:
            self.font = None

    def to_dict(self) -> dict:
        result = {
            TextOverlay.TEXT_FIELD: self.text,
            TextOverlay.X_ABSOLUTE_FILED: self.x_absolute,
            TextOverlay.Y_ABSOLUTE_FILED: self.y_absolute,
        }
        if self.font is not None:
            result[TextOverlay.FONT_FIELD] = self.font.to_dict()

        return result
