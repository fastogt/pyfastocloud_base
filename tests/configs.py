#!/usr/bin/env python3
import json
from typing import Optional
import unittest

from pyfastocloud_base.constants import StreamType, StreamLogLevel, OutputUri, InputUri
from pyfastocloud_base.ml.machine_learning import MachineLearning
from pyfastocloud_base.streams.config import (
    ProxyConfig,
    ProxyVodConfig,
    RelayConfig,
    EncodeConfig,
    CodEncodeConfig,
    CodRelayConfig,
    CatchupConfig,
    TimeShiftRecordConfig,
    TimeshiftPlayerConfig,
    CvDataConfig,
)


class ConfigsTests(unittest.TestCase):
    maxDiff: Optional[int] = None

    def _to_json_from_json(self, data):
        json_raw = json.dumps(data.to_dict())
        dic = json.loads(json_raw)
        self.assertEqual(dic, data.to_dict())

    def test_proxy(self):

        data = {
            "sid": "1234",
            "output": [
                OutputUri(id=11, uri="https://some.tv/live/secure/chunklist.m3u8")
            ],
        }
        origin = {
            ProxyConfig.ID_FIELD: "1234",
            ProxyConfig.OUTPUT_FIELD: [
                {"id": 11, "uri": "https://some.tv/live/secure/chunklist.m3u8"}
            ],
            ProxyConfig.TYPE_FIELD: StreamType.PROXY.value,
        }
        proxy = ProxyConfig(**data)
        self.assertTrue(proxy.is_valid())
        self.assertEqual(proxy.id, data["sid"])
        self.assertEqual(proxy.output, data["output"])
        self.assertEqual(proxy.type(), StreamType.PROXY.value)
        self.assertEqual(proxy.to_dict(), origin)
        self._to_json_from_json(proxy)

    def test_proxy_vod(self):
        data = {
            "sid": "1234",
            "output": [
                OutputUri(id=12, uri="https://some.tv/live/secure/chunklist.m3u8"),
            ],
        }
        origin = {
            ProxyVodConfig.ID_FIELD: "1234",
            ProxyVodConfig.OUTPUT_FIELD: [
                {"id": 12, "uri": "https://some.tv/live/secure/chunklist.m3u8"}
            ],
            ProxyVodConfig.TYPE_FIELD: StreamType.VOD_PROXY.value,
        }
        proxy = ProxyVodConfig(**data)
        self.assertTrue(proxy.is_valid())
        self.assertEqual(proxy.id, data["sid"])
        self.assertEqual(proxy.output, data["output"])
        self.assertEqual(proxy.type(), StreamType.VOD_PROXY.value)
        self.assertEqual(proxy.to_dict(), origin)
        self._to_json_from_json(proxy)

    def test_relay(self):
        data = {
            "sid": "1234",
            "output": [
                OutputUri(id=12, uri="https://some.tv/live/secure/chunklist.m3u8"),
            ],
            "feedback_dir": "~/logs/1",
            "data_directory": "~/data/da",
            "loop": False,
            "log_level": StreamLogLevel.LOG_LEVEL_DEBUG.value,
            "selected_input": 13,
            "have_video": True,
            "have_audio": True,
            "restart_attempts": 2,
            "audio_tracks_count": 3,
            "input": [
                InputUri(id=13, uri="https://some.tv/live/secure/chunk2li1st.m3u8")
            ],
        }
        origin = {
            RelayConfig.ID_FIELD: "1234",
            RelayConfig.OUTPUT_FIELD: [
                {"id": 12, "uri": "https://some.tv/live/secure/chunklist.m3u8"}
            ],
            RelayConfig.FEEDBACK_DIR_FIELD: "~/logs/1",
            RelayConfig.DATA_DIR_FIELD: "~/data/da",
            RelayConfig.LOOP_FIELD: False,
            RelayConfig.LOG_LEVEL_FIELD: StreamLogLevel.LOG_LEVEL_DEBUG.value,
            RelayConfig.SELECTED_INPUT_FIELD: 13,
            RelayConfig.HAVE_VIDEO_FIELD: True,
            RelayConfig.HAVE_AUDIO_FIELD: True,
            RelayConfig.RESTART_ATTEMPTS_FIELD: 2,
            RelayConfig.AUDIO_TRACKS_COUNT_FIELD: 3,
            RelayConfig.INPUT_FIELD: [
                {"id": 13, "uri": "https://some.tv/live/secure/chunk2li1st.m3u8"}
            ],
            RelayConfig.TYPE_FIELD: StreamType.RELAY,
        }
        relay = RelayConfig(**data)
        self.assertTrue(relay.is_valid())
        self.assertEqual(relay.id, data["sid"])
        self.assertEqual(relay.type(), StreamType.RELAY.value)
        self.assertEqual(relay.to_dict(), origin)
        self._to_json_from_json(relay)

    def test_encode(self):
        data = {
            "sid": "22223",
            "output": [
                OutputUri(id=12, uri="https://some.tv/live/secure/chunklist.m3u8"),
            ],
            "feedback_dir": "~/logs/1",
            "data_directory": "~/data/da",
            "loop": False,
            "log_level": StreamLogLevel.LOG_LEVEL_DEBUG.value,
            "selected_input": 133,
            "have_video": True,
            "have_audio": False,
            "relay_video": True,
            "relay_audio": True,
            "restart_attempts": 2,
            "audio_tracks_count": 3,
            "input": [
                InputUri(id=13, uri="https://some.tv/live/secure/chunk2li1st.m3u8")
            ],
            "audio_codec": "faac",
            "video_codec": "x264enc",
            "deinterlace": False,
            "resample": True,
            "have_audio": False,
            "have_video": True,
            "volume": 0.3,
        }
        origin = {
            EncodeConfig.ID_FIELD: "22223",
            EncodeConfig.OUTPUT_FIELD: [
                {"id": 12, "uri": "https://some.tv/live/secure/chunklist.m3u8"}
            ],
            EncodeConfig.FEEDBACK_DIR_FIELD: "~/logs/1",
            EncodeConfig.DATA_DIR_FIELD: "~/data/da",
            EncodeConfig.LOOP_FIELD: False,
            EncodeConfig.LOG_LEVEL_FIELD: StreamLogLevel.LOG_LEVEL_DEBUG.value,
            EncodeConfig.SELECTED_INPUT_FIELD: 133,
            EncodeConfig.HAVE_VIDEO_FIELD: True,
            EncodeConfig.HAVE_AUDIO_FIELD: False,
            EncodeConfig.RELAY_VIDEO_FIELD: True,
            EncodeConfig.RELAY_AUDIO_FIELD: True,
            EncodeConfig.RESTART_ATTEMPTS_FIELD: 2,
            EncodeConfig.AUDIO_TRACKS_COUNT_FIELD: 3,
            EncodeConfig.INPUT_FIELD: [
                {"id": 13, "uri": "https://some.tv/live/secure/chunk2li1st.m3u8"}
            ],
            EncodeConfig.AUDIO_CODEC_FIELD: "faac",
            EncodeConfig.VIDEO_CODEC_FIELD: "x264enc",
            EncodeConfig.DEINTERLACE_FIELD: False,
            EncodeConfig.RESAMPLE_FIELD: True,
            EncodeConfig.HAVE_AUDIO_FIELD: False,
            EncodeConfig.HAVE_VIDEO_FIELD: True,
            EncodeConfig.VOLUME_FIELD: 0.3,
            EncodeConfig.TYPE_FIELD: StreamType.ENCODE.value,
        }
        encode = EncodeConfig(**data)
        self.assertTrue(encode.is_valid())
        self.assertEqual(encode.id, data["sid"])
        self.assertEqual(encode.output, data["output"])
        self.assertEqual(encode.to_dict(), origin)
        self._to_json_from_json(encode)

    def test_catchup(self):
        data = {
            "sid": "1234",
            "output": [
                OutputUri(id=12, uri="https://some.tv/live/secure/chunklist.m3u8"),
            ],
            "feedback_dir": "~/logs/1",
            "data_directory": "~/data/da",
            "log_level": StreamLogLevel.LOG_LEVEL_DEBUG.value,
            "loop": False,
            "selected_input": 131,
            "have_video": True,
            "have_audio": False,
            "restart_attempts": 21,
            "audio_tracks_count": 8,
            "input": [
                InputUri(id=131, uri="https://some.tv/live/secure/chunk2li1st.m3u8")
            ],
            "timeshift_chunk_duration": 233,
            "timeshift_chunk_life_time": 34,
            "timeshift_dir": "/home/sasha/aa",
            "start": 123124,
            "stop": 5434545,
        }
        origin = {
            CatchupConfig.ID_FIELD: "1234",
            CatchupConfig.OUTPUT_FIELD: [
                {"id": 12, "uri": "https://some.tv/live/secure/chunklist.m3u8"}
            ],
            CatchupConfig.FEEDBACK_DIR_FIELD: "~/logs/1",
            CatchupConfig.DATA_DIR_FIELD: "~/data/da",
            CatchupConfig.LOG_LEVEL_FIELD: StreamLogLevel.LOG_LEVEL_DEBUG.value,
            CatchupConfig.LOOP_FIELD: False,
            CatchupConfig.SELECTED_INPUT_FIELD: 131,
            CatchupConfig.HAVE_VIDEO_FIELD: True,
            CatchupConfig.HAVE_AUDIO_FIELD: False,
            CatchupConfig.RESTART_ATTEMPTS_FIELD: 21,
            CatchupConfig.AUDIO_TRACKS_COUNT_FIELD: 8,
            CatchupConfig.INPUT_FIELD: [
                {"id": 131, "uri": "https://some.tv/live/secure/chunk2li1st.m3u8"}
            ],
            CatchupConfig.TIMESHIFT_CHUNK_DURATION: 233,
            CatchupConfig.TIMESHIFT_CHUNK_LIFE_TIME: 34,
            CatchupConfig.TIMESHIFT_DIR: "/home/sasha/aa",
            CatchupConfig.AUTO_EXIT_TIME_FIELD: CatchupConfig.calc_auto_exit_time_in_seconds(
                123124, 5434545
            ).to_dict(),
            CatchupConfig.TYPE_FIELD: StreamType.CATCHUP.value,
        }
        catchup = CatchupConfig(**data)
        self.assertTrue(catchup.is_valid())
        self.assertEqual(catchup.id, data["sid"])
        self.assertEqual(catchup.output, data["output"])
        self.assertEqual(catchup.to_dict(), origin)
        self._to_json_from_json(catchup)

    def test_timeshift_record(self):
        data = {
            "sid": "1234",
            "output": [
                OutputUri(id=12, uri="https://some.tv/live/secure/chunklist.m3u8"),
            ],
            "feedback_dir": "~/logs/1",
            "data_directory": "~/data/da",
            "log_level": StreamLogLevel.LOG_LEVEL_DEBUG.value,
            "loop": False,
            "selected_input": 131,
            "have_video": True,
            "have_audio": False,
            "restart_attempts": 21,
            "audio_tracks_count": 8,
            "input": [
                InputUri(id=131, uri="https://some.tv/live/secure/chunk2li1st.m3u8")
            ],
            "timeshift_chunk_duration": 233,
            "timeshift_chunk_life_time": 34,
            "timeshift_dir": "/home/sasha/aa",
        }
        origin = {
            TimeShiftRecordConfig.ID_FIELD: "1234",
            TimeShiftRecordConfig.OUTPUT_FIELD: [
                {"id": 12, "uri": "https://some.tv/live/secure/chunklist.m3u8"}
            ],
            TimeShiftRecordConfig.FEEDBACK_DIR_FIELD: "~/logs/1",
            TimeShiftRecordConfig.DATA_DIR_FIELD: "~/data/da",
            TimeShiftRecordConfig.LOG_LEVEL_FIELD: StreamLogLevel.LOG_LEVEL_DEBUG.value,
            TimeShiftRecordConfig.LOOP_FIELD: False,
            TimeShiftRecordConfig.SELECTED_INPUT_FIELD: 131,
            TimeShiftRecordConfig.HAVE_VIDEO_FIELD: True,
            TimeShiftRecordConfig.HAVE_AUDIO_FIELD: False,
            TimeShiftRecordConfig.RESTART_ATTEMPTS_FIELD: 21,
            TimeShiftRecordConfig.AUDIO_TRACKS_COUNT_FIELD: 8,
            TimeShiftRecordConfig.INPUT_FIELD: [
                {"id": 131, "uri": "https://some.tv/live/secure/chunk2li1st.m3u8"}
            ],
            TimeShiftRecordConfig.TIMESHIFT_CHUNK_DURATION: 233,
            TimeShiftRecordConfig.TIMESHIFT_CHUNK_LIFE_TIME: 34,
            TimeShiftRecordConfig.TIMESHIFT_DIR: "/home/sasha/aa",
            TimeShiftRecordConfig.TYPE_FIELD: StreamType.TIMESHIFT_RECORDER.value,
        }
        record = TimeShiftRecordConfig(**data)
        self.assertTrue(record.is_valid())
        self.assertEqual(record.id, data["sid"])
        self.assertEqual(record.output, data["output"])
        self.assertEqual(record.to_dict(), origin)
        self._to_json_from_json(record)

    def test_timeshift_player(self):
        data = {
            "sid": "1234",
            "output": [
                OutputUri(id=12, uri="https://some.tv/live/secure/chunklist.m3u8"),
            ],
            "feedback_dir": "~/logs/1",
            "data_directory": "~/data/da",
            "log_level": StreamLogLevel.LOG_LEVEL_DEBUG.value,
            "loop": False,
            "selected_input": 131,
            "have_video": True,
            "have_audio": False,
            "restart_attempts": 21,
            "audio_tracks_count": 8,
            "input": [
                InputUri(id=131, uri="https://some.tv/live/secure/chunk2li1st.m3u8")
            ],
            "timeshift_delay": 34,
            "timeshift_dir": "/home/sasha/aa",
        }
        origin = {
            TimeshiftPlayerConfig.ID_FIELD: "1234",
            TimeshiftPlayerConfig.OUTPUT_FIELD: [
                {"id": 12, "uri": "https://some.tv/live/secure/chunklist.m3u8"}
            ],
            TimeshiftPlayerConfig.FEEDBACK_DIR_FIELD: "~/logs/1",
            TimeshiftPlayerConfig.DATA_DIR_FIELD: "~/data/da",
            TimeshiftPlayerConfig.LOG_LEVEL_FIELD: StreamLogLevel.LOG_LEVEL_DEBUG.value,
            TimeshiftPlayerConfig.LOOP_FIELD: False,
            TimeshiftPlayerConfig.SELECTED_INPUT_FIELD: 131,
            TimeshiftPlayerConfig.HAVE_VIDEO_FIELD: True,
            TimeshiftPlayerConfig.HAVE_AUDIO_FIELD: False,
            TimeshiftPlayerConfig.RESTART_ATTEMPTS_FIELD: 21,
            TimeshiftPlayerConfig.AUDIO_TRACKS_COUNT_FIELD: 8,
            TimeshiftPlayerConfig.INPUT_FIELD: [
                {"id": 131, "uri": "https://some.tv/live/secure/chunk2li1st.m3u8"}
            ],
            TimeshiftPlayerConfig.TIMESHIFT_DIR: "/home/sasha/aa",
            TimeshiftPlayerConfig.TIMESHIFT_DELAY: 34,
            TimeshiftPlayerConfig.TYPE_FIELD: StreamType.TIMESHIFT_PLAYER.value,
        }
        record = TimeshiftPlayerConfig(**data)
        self.assertTrue(record.is_valid())
        self.assertEqual(record.id, data["sid"])
        self.assertEqual(record.output, data["output"])
        self.assertEqual(record.to_dict(), origin)
        self._to_json_from_json(record)

    def test_cod_relay(self):
        data = {
            "sid": "1234",
            "output": [
                OutputUri(id=12, uri="https://some.tv/live/secure/chunklist.m3u8"),
            ],
            "feedback_dir": "~/logs/1",
            "data_directory": "~/data/da",
            "log_level": StreamLogLevel.LOG_LEVEL_DEBUG.value,
            "loop": False,
            "selected_input": 131,
            "have_video": True,
            "have_audio": False,
            "restart_attempts": 21,
            "audio_tracks_count": 8,
            "input": [
                InputUri(id=131, uri="https://some.tv/live/secure/chunk2li1st.m3u8")
            ],
        }
        origin = {
            CodRelayConfig.ID_FIELD: "1234",
            CodRelayConfig.OUTPUT_FIELD: [
                {"id": 12, "uri": "https://some.tv/live/secure/chunklist.m3u8"}
            ],
            CodRelayConfig.FEEDBACK_DIR_FIELD: "~/logs/1",
            CodRelayConfig.DATA_DIR_FIELD: "~/data/da",
            CodRelayConfig.LOG_LEVEL_FIELD: StreamLogLevel.LOG_LEVEL_DEBUG.value,
            CodRelayConfig.LOOP_FIELD: False,
            CodRelayConfig.SELECTED_INPUT_FIELD: 131,
            CodRelayConfig.HAVE_VIDEO_FIELD: True,
            CodRelayConfig.HAVE_AUDIO_FIELD: False,
            CodRelayConfig.RESTART_ATTEMPTS_FIELD: 21,
            CodRelayConfig.AUDIO_TRACKS_COUNT_FIELD: 8,
            CodRelayConfig.INPUT_FIELD: [
                {"id": 131, "uri": "https://some.tv/live/secure/chunk2li1st.m3u8"}
            ],
            CodRelayConfig.TYPE_FIELD: StreamType.COD_RELAY.value,
        }
        relay = CodRelayConfig(**data)
        self.assertTrue(relay.is_valid())
        self.assertEqual(relay.id, data["sid"])
        self.assertEqual(relay.output, data["output"])
        self.assertEqual(relay.to_dict(), origin)
        self._to_json_from_json(relay)

    def test_cod_encode(self):
        data = {
            "sid": "22223dfe",
            "output": [
                OutputUri(id=12, uri="https://some.tv/live/secure/chunklist.m3u8"),
            ],
            "feedback_dir": "~/logs/1",
            "data_directory": "~/data/da",
            "log_level": StreamLogLevel.LOG_LEVEL_DEBUG.value,
            "loop": False,
            "selected_input": 13213,
            "have_video": True,
            "have_audio": False,
            "restart_attempts": 2,
            "audio_tracks_count": 3,
            "input": [InputUri(id=13213, uri="srt://:7001")],
            "relay_video": True,
            "relay_audio": False,
            "deinterlace": True,
            "resample": True,
            "volume": 1.3,
            "video_codec": "x243enc",
            "audio_codec": "voaacenc",
        }
        origin = {
            CodEncodeConfig.ID_FIELD: "22223dfe",
            CodEncodeConfig.OUTPUT_FIELD: [
                {"id": 12, "uri": "https://some.tv/live/secure/chunklist.m3u8"}
            ],
            CodEncodeConfig.FEEDBACK_DIR_FIELD: "~/logs/1",
            CodEncodeConfig.DATA_DIR_FIELD: "~/data/da",
            CodEncodeConfig.LOG_LEVEL_FIELD: StreamLogLevel.LOG_LEVEL_DEBUG.value,
            CodEncodeConfig.LOOP_FIELD: False,
            CodEncodeConfig.SELECTED_INPUT_FIELD: 13213,
            CodEncodeConfig.HAVE_VIDEO_FIELD: True,
            CodEncodeConfig.HAVE_AUDIO_FIELD: False,
            CodEncodeConfig.RESTART_ATTEMPTS_FIELD: 2,
            CodEncodeConfig.AUDIO_TRACKS_COUNT_FIELD: 3,
            CodEncodeConfig.INPUT_FIELD: [{"id": 13213, "uri": "srt://:7001"}],
            CodEncodeConfig.RELAY_VIDEO_FIELD: True,
            CodEncodeConfig.RELAY_AUDIO_FIELD: False,
            CodEncodeConfig.DEINTERLACE_FIELD: True,
            CodEncodeConfig.RESAMPLE_FIELD: True,
            CodEncodeConfig.VOLUME_FIELD: 1.3,
            CodEncodeConfig.VIDEO_CODEC_FIELD: "x243enc",
            CodEncodeConfig.AUDIO_CODEC_FIELD: "voaacenc",
            CodEncodeConfig.TYPE_FIELD: StreamType.COD_ENCODE.value,
        }
        encode = CodEncodeConfig(**data)
        self.assertTrue(encode.is_valid())
        self.assertEqual(encode.id, data["sid"])
        self.assertEqual(encode.output, data["output"])
        self.assertEqual(encode.to_dict(), origin)
        self._to_json_from_json(encode)

    def test_cvdata(self):
        machine_learning = {
            MachineLearning.BACKEND_FIELD: 0,
            MachineLearning.MODEL_URL_FIELD: "file:///opt/nvidia/deepstream/deepstream-5.0/sources/objectDetector_Yolo/config_infer_primary_yoloV3_tiny.txt",
            MachineLearning.TRACKING_FIELD: False,
            MachineLearning.DUMP_FIELD: True,
            MachineLearning.CLASS_ID_FIELD: 1,
            MachineLearning.OVERLAY_FIELD: False,
        }
        ml = MachineLearning.make_entry(machine_learning)

        data = {
            "sid": "22223dfe",
            "output": [
                OutputUri(id=12, uri="https://some.tv/live/secure/chunklist.m3u8"),
            ],
            "feedback_dir": "~/logs/1",
            "data_directory": "~/data/da",
            "log_level": StreamLogLevel.LOG_LEVEL_ALERT,
            "loop": False,
            "selected_input": 13213,
            "have_video": True,
            "have_audio": False,
            "restart_attempts": 2,
            "audio_tracks_count": 3,
            "input": [InputUri(id=13213, uri="srt://:7001")],
            "relay_video": True,
            "relay_audio": False,
            "deinterlace": True,
            "resample": True,
            "volume": 1.3,
            "video_codec": "voaacenc",
            "audio_codec": "x264enc",
            "machine_learning": machine_learning,
        }

        origin = {
            CvDataConfig.ID_FIELD: "22223dfe",
            CvDataConfig.OUTPUT_FIELD: [
                {"id": 12, "uri": "https://some.tv/live/secure/chunklist.m3u8"}
            ],
            CvDataConfig.FEEDBACK_DIR_FIELD: "~/logs/1",
            CvDataConfig.DATA_DIR_FIELD: "~/data/da",
            CvDataConfig.LOG_LEVEL_FIELD: StreamLogLevel.LOG_LEVEL_ALERT,
            CvDataConfig.LOOP_FIELD: False,
            CvDataConfig.SELECTED_INPUT_FIELD: 13213,
            CvDataConfig.HAVE_VIDEO_FIELD: True,
            CvDataConfig.HAVE_AUDIO_FIELD: False,
            CvDataConfig.RESTART_ATTEMPTS_FIELD: 2,
            CvDataConfig.AUDIO_TRACKS_COUNT_FIELD: 3,
            CvDataConfig.INPUT_FIELD: [{"id": 13213, "uri": "srt://:7001"}],
            CvDataConfig.RELAY_VIDEO_FIELD: True,
            CvDataConfig.RELAY_AUDIO_FIELD: False,
            CvDataConfig.DEINTERLACE_FIELD: True,
            CvDataConfig.RESAMPLE_FIELD: True,
            CvDataConfig.VOLUME_FIELD: 1.3,
            CvDataConfig.VIDEO_CODEC_FIELD: "voaacenc",
            CvDataConfig.AUDIO_CODEC_FIELD: "x264enc",
            CvDataConfig.MACHINE_LEARNING_FIELD: machine_learning,
            CvDataConfig.TYPE_FIELD: StreamType.CV_DATA,
        }

        self.assertEqual(ml.to_dict(), machine_learning)
        encode = CvDataConfig(**data)
        self.assertTrue(encode.is_valid())
        self.assertEqual(encode.id, data["sid"])
        self.assertEqual(encode.output, data["output"])
        self.assertEqual(encode.to_dict(), origin)
        self._to_json_from_json(encode)


if __name__ == "__main__":
    unittest.main()
